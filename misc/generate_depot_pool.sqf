/*
 * A tool for generating depot items. Each call puts the code for the depot
 * item into the clipboard.
 *
 * Invoke this by passing a group. For example, you can put a bunch of units in
 * a group in zeus and double click on any of them and run the following:
 *
 *     [group this] call compile preprocessFileLineNumbers "misc\generate_depot_pool.sqf"
 *
 * The blocks of code copied to your clipboard should be added as elements in a
 * list somewhere, see `depot_opfor_pool.sqf` as an example. That file is just
 * a bunch of these in a big list.
 *
 * Changes to crew are ignored. Vehicles are created with their default crew.
 */
#include "..\ineptifex\data.h"

params ["_group"];

private _out = [];

private _units = units _group;
private _units_inside = [];
private _units_outside = [];

{
    if (vehicle _x isEqualTo _x) then
    {
        _units_outside pushBack _x;
    }
    else
    {
        _units_inside pushBack _x;
    };
}
forEach _units;

private _containing_vehicles = [];

{
    if ((_containing_vehicles find (vehicle _x)) == -1) then
    {
        _containing_vehicles pushBack (vehicle _x);
    };
}
forEach _units_inside;

private _createme = _containing_vehicles + _units_outside;
private _createme_classnames = [_createme, {typeOf _x}] call INEPT_fnc_applyToAll;

_out pushBack (format ["[ %1", _createme_classnames]);

{
    private _info =
        [ _x
        , UNIT_INFO_ACCURATE
        , UNIT_INFO_WITH_CARGO
        ] call INEPT_fnc_getUnitInfo;
    _out pushBack (format ["  %1 %2", if (_forEachIndex == 0) then {", ["} else {"  ,"}, _info]);
}
forEach _createme;

_out pushBack "    ]";
_out pushBack "  , 1";
_out pushBack "] call cy_fnc_DepotItem_init";

copyToClipboard (_out joinString "
");

diag_log (format ["Copied item for %1 to clipboard!", _createme_classnames]);
