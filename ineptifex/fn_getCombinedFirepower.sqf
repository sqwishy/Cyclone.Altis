/* INEPT_fnc_getCombinedFirepower
 *
 * Parameters:
 *  0: An array of unit info instances
 *
 * Specification:
 *  Returns a firepower that represents the combination of all the unit infos
 *  passed in argument 0.
 *  The idea here is that the unit infos in argument 0 are part of a group or
 *  formation or something. The firepower constitution and lethality fields are
 *  simply summed, but mobility and stealth are determined by the minimum of
 *  the group. The reasoning there being that if one guy is really slow or
 *  sticks out a lot, everyone will have to wait for him or he will expose the
 *  entire group.
 *  Cargo is included, except for the calculation of mobility and stealth. They
 *  go as fast as whatever is transporting them, and they are probably
 *  concealed by it as well.
 */

#define DEBUG_VAR "INEPT_fnc_getFirepower_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_infos) = ARGUMENT_0;

VAR(_fp) = NEW_FIREPOWER();
if ([] isEqualTo (_infos)) exitWith {_fp};

{
    VAR(_x_fp) = [_x] call INEPT_fnc_getFirepower;
    _fp set [FIREPOWER_I,  (_fp select FIREPOWER_I)   + (_x_fp select FIREPOWER_I)];
    _fp set [FIREPOWER_AI, (_fp select FIREPOWER_AI)  + (_x_fp select FIREPOWER_AI)];
    _fp set [FIREPOWER_R,  (_fp select FIREPOWER_R)   + (_x_fp select FIREPOWER_R)];
    _fp set [FIREPOWER_AR, (_fp select FIREPOWER_AR)  + (_x_fp select FIREPOWER_AR)];
    _fp set [FIREPOWER_A,  (_fp select FIREPOWER_A)   + (_x_fp select FIREPOWER_A)];
    _fp set [FIREPOWER_AA, (_fp select FIREPOWER_AA)  + (_x_fp select FIREPOWER_AA)];
    if (_fp select FIREPOWER_MOBILITY == -1
        or {_fp select FIREPOWER_MOBILITY > _x_fp select FIREPOWER_MOBILITY}) then
    {
        _fp set [FIREPOWER_MOBILITY, _x_fp select FIREPOWER_MOBILITY];
    };
    if (_fp select FIREPOWER_STEALTH == -1
        or {_fp select FIREPOWER_STEALTH > _x_fp select FIREPOWER_STEALTH}) then
    {
        _fp set [FIREPOWER_STEALTH, _x_fp select FIREPOWER_STEALTH];
    };
    // This does not recurse, so it does not support cargo inside of cargo,
    // this is probably a good thing
    // This also ignores mobility and stealth, those are omitted as a
    // consequnce of these things being cargo. The firepower is still added to
    // the result since we assume that the infantry or whatever could get out
    // and do whatever whenever.
    {
        VAR(_cargo_fp) = [_x] call INEPT_fnc_getFirepower;
        _fp set [FIREPOWER_I,  (_fp select FIREPOWER_I)   + (_cargo_fp select FIREPOWER_I)];
        _fp set [FIREPOWER_AI, (_fp select FIREPOWER_AI)  + (_cargo_fp select FIREPOWER_AI)];
        _fp set [FIREPOWER_R,  (_fp select FIREPOWER_R)   + (_cargo_fp select FIREPOWER_R)];
        _fp set [FIREPOWER_AR, (_fp select FIREPOWER_AR)  + (_cargo_fp select FIREPOWER_AR)];
        _fp set [FIREPOWER_A,  (_fp select FIREPOWER_A)   + (_cargo_fp select FIREPOWER_A)];
        _fp set [FIREPOWER_AA, (_fp select FIREPOWER_AA)  + (_cargo_fp select FIREPOWER_AA)];
    }
    forEach (_x select UNIT_INFO_CARGO);
}
forEach _infos;

// Certain qualities, like stealth and mobility, are not cumulative,
// they tend to worsen with larger groups, this is why the cargo thing is important
VAR(_logistics_penalty) = 1 / ((count _infos) ^ 0.1);
_fp set [FIREPOWER_MOBILITY, _logistics_penalty * (_fp select FIREPOWER_MOBILITY)];
_fp set [FIREPOWER_STEALTH, _logistics_penalty * (_fp select FIREPOWER_STEALTH)];
_fp
