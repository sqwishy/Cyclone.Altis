/* INEPT_fnc_getUnitInfo
 *
 * Parameters:
 *  0: A unit object, like an actual arma unit
 *  1: Either UNIT_INFO_APPEARANCE or UNIT_INFO_ACCURATE
 *  2: Either UNIT_INFO_WITH_CARGO or UNIT_INFO_NO_CARGO
 *
 * Specification:
 *  So this will interrogate the given unit and his/her/its equipment and
 *  create a unit info instance and return it. It does a lot of funky things to
 *  try to make a good guess when you use UNIT_INFO_APPEARANCE, because it
 *  is supposed to simulate basically what you can just see from a visual
 *  inspection of the unit.
 */

#define DEBUG_VAR "INEPT_fnc_getUnitInfo_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_unit) = ARGUMENT_0;
VAR(_info_accuracy) = ARGUMENT_1;
VAR(_include_cargo) = ARGUMENT_2;

LOGVAR(_unit);
LOGVAR(_info_accuracy);
LOGVAR(_include_cargo);

VAR(_info) = NEW_UNIT_INFO(typeOf _unit);
if (isNull _unit) exitWith
{
    ["Null object passed as unit"] call BIS_fnc_error;
    _info
};

VAR(_equipment) = _info select UNIT_INFO_EQUIPMENT;
VAR(_weapons) = _equipment select EQUIPMENT_WEAPONS;
VAR(_magazines) = _equipment select EQUIPMENT_MAGAZINES;

// Figure out what weapons are on the guy

if (_unit isKindOf "Man") then
{
    // Ignore the sidearm on peepz because it's lame and nobody cares (todo,
    // this is kind of janky - one day, somebody may care)
    if (primaryWeapon _unit != "") then
    {
        _weapons pushBack (primaryWeapon _unit);
    };
    if (secondaryWeapon _unit != "") then
    {
        _weapons pushBack (secondaryWeapon _unit);
    };
}
else
{
    _weapons append (weapons _unit);
    _weapons append (_unit weaponsTurret [-1]);
    {
        _weapons append (_unit weaponsTurret _x);
    }
    forEach (allTurrets [_unit, false]);
};

LOGVAR(_weapons);

if (_info_accuracy == UNIT_INFO_APPEARANCE) then
{
    {
        VAR(_weapon_classname) = _x;
        VAR(_muzzle_mags) = [_weapon_classname] call INEPT_fnc_magazinesForWeaponCfg;
        [
            _muzzle_mags,
            {not ((_x select PAIR_SECOND) isEqualTo [])}
        ]
        call INEPT_fnc_conditionalSelectIP;
        {
            // Alright, lets pick the magazine with the ammo that has the
            // greatest `hit` value ...
            // Selecting without discrimination is a horrible idea because what
            // ends up happening is that we see a guy with a grenade launcher
            // and we assume the only ammo he has is 50 flares.
            VAR(_mags) = _x select PAIR_SECOND;
            LOGVAR(_mags);
            VAR(_mag) = _mags select (
                [_mags,
                {
                    // The random here is to prevent selecting the same
                    // magazine every time in cases where multiple magazines
                    // have same hit value.
                    getNumber (
                        configFile
                        >> "CfgAmmo"
                        >> getText (configFile >> "CfgMagazines" >> _x >> "ammo")
                        >> "hit"
                    ) + random 0.5
                }
                ] call INEPT_fnc_indexOfBest
                );
            LOGVAR(_mag);
            // fixme, This is pretty awful
            VAR(_num) = switch (true) do
                {
                case (0 == (getNumber (configFile >> "CfgMagazines" >> _mag >> "type"))):
                {
                    // A magazine for a like a vehicle that is not a person thingy
                    LOGMSG("this is a vehicle mag");
                    2
                };
                // this may be a bad way of determining if this is a launchy
                // rocket thingy, or, more particularly, if it requires a
                // backpack
                case (((3 * 256) <= (getNumber (configFile >> "CfgMagazines" >> _mag >> "type")))
                      and {_unit isKindOf "Man"}
                      and {backpack _unit == ""}):
                {
                    LOGMSG("dude aint got no backpack fur his launcher ammo");
                    // Assume there's one missile in the tube, but no other
                    // rounds because the guy can't hold them in his pockets
                    1
                };
                default
                {
                    LOGMSG("doing this the hard way");
                    VAR(_mass) = getNumber (configFile >> "CfgMagazines" >> _mag >> "mass");
                    LOGVAR(_mass);
                    switch (true) do
                    {
                    case (_mass >= 40):
                    {
                        1 + floor random 2
                    };
                    case (_mass >= 20):
                    {
                        3 + floor random 5
                    };
                    default
                    {
                        6 + floor random 7
                    };
                    };
                };
                };
            LOGVAR(_num);
            while {_num > 0} do
            {
                _magazines pushBack _mag;
                _num = _num - 1;
            };
        }
        forEach _muzzle_mags;
    }
    forEach _weapons;
}
else
{
    _magazines append (magazines _unit);
    _magazines append (primaryWeaponMagazine _unit);
    _magazines append (secondaryWeaponMagazine _unit);
    _magazines append (_unit magazinesTurret [-1]);
    if !(_unit isKindOf "Man") then
    {
        {
            _magazines append (_unit magazinesTurret _x);
        }
        forEach (allTurrets [_unit, false]);
    };
};

if (_include_cargo == UNIT_INFO_WITH_CARGO) then
{
    VAR(_cargo) = _info select UNIT_INFO_CARGO;

    VAR(_crew) = crew _unit;
    if !(_crew isEqualTo [_unit]) then
    {
        {
            _cargo pushBack (
                [_x, _info_accuracy, UNIT_INFO_NO_CARGO] call INEPT_fnc_getUnitInfo
                );
        }
        forEach _crew;
    };
};

LOGVAR(_info);
_info
