// See INEPT_fnc_buildBehaviorLogics

#define DEBUG_VAR "INEPT_fnc_behaviorDefendUninstall_is_verbose"

#include "common\defines.h"
#include "data.h"
#include "behaviordata.h"

LOGENTRY();

params ["_ao", "_pawn"];
private _wp = [_pawn, DEFEND_WP_NAME] call INEPT_fnc_findWaypointByName;
if !(_wp isEqualTo NULL_WP()) then
{
    deleteWaypoint _wp;
};
