#define VAR(NAME) private #NAME;\
                    NAME

// Argument handling

#define ARGUMENT_COUNT (count _this)

#define ARGUMENT _this
#define ARGUMENT_N(IDX) (_this select IDX)
#define ARGUMENT_0 ARGUMENT_N(0)
#define ARGUMENT_1 ARGUMENT_N(1)
#define ARGUMENT_2 ARGUMENT_N(2)
#define ARGUMENT_3 ARGUMENT_N(3)
#define ARGUMENT_4 ARGUMENT_N(4)
#define ARGUMENT_5 ARGUMENT_N(5)
#define ARGUMENT_6 ARGUMENT_N(6)
#define ARGUMENT_7 ARGUMENT_N(7)
#define ARGUMENT_8 ARGUMENT_N(8)
#define ARGUMENT_9 ARGUMENT_N(9)

#define OPT_ARGUMENT_N(DEFAULT, N) \
    if (count _this > N) then { _this select N } else { DEFAULT }
#define OPT_ARGUMENT_0(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 0);
#define OPT_ARGUMENT_1(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 1);
#define OPT_ARGUMENT_2(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 2);
#define OPT_ARGUMENT_3(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 3);
#define OPT_ARGUMENT_4(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 4);
#define OPT_ARGUMENT_5(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 5);
#define OPT_ARGUMENT_6(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 6);
#define OPT_ARGUMENT_7(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 7);
#define OPT_ARGUMENT_8(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 8);
#define OPT_ARGUMENT_9(DEFAULT) OPT_ARGUMENT_N(DEFAULT, 9);

// Logging

// N.B.: _fnc_scriptName is unreliable
#define WHEREAMI() \
    format [ \
        "%1:%2", \
        if (isNil "_fnc_scriptName") then { __FILE__ } else { _fnc_scriptName }, \
        __LINE__ + 1 \
        ]

#ifdef DEBUG_VAR
    #define LOGGING_ENABLED() (!isNil DEBUG_VAR)
#else
    #define LOGGING_ENABLED() (true)
#endif

#define LOGVAR(NAME) \
    if (LOGGING_ENABLED()) then \
    { \
        diag_log format ["VAR:%1 - %2 = %3", __LINE__ + 1, #NAME, NAME] \
    }
//#define LOGVAR(NAME)

//#define LOGTICK() \
//    if (LOGGING_ENABLED()) then \
//    { \
//        diag_log format ["TICK:%1 - %2", __LINE__ + 1, diag_tickTime] \
//    }

// This is a bit more helpful than the above, but it only works if LOGENTRY()
// was called so that _lastticktime can be placed in the scope, maybe a new
// directive should be added specifically for initilizating _lastticktime?
#define LOGTICK() \
    if (LOGGING_ENABLED()) then \
    { \
        diag_log format ["TICK:%1 - %2 (%3)", __LINE__ + 1, diag_tickTime, diag_tickTime - _lastticktime]; \
        _lastticktime = diag_tickTime; \
    }

#define LOGENTRY() \
    VAR(_lastticktime) = diag_tickTime; \
    if (LOGGING_ENABLED()) then \
    { \
        diag_log format ["ENTRY:%1", WHEREAMI()] \
    }

#define LOGMSG(msg) \
    if (LOGGING_ENABLED()) then \
    { \
        diag_log format ["MSG:%1 - %2", __LINE__ + 1, msg] \
    }

// Mutexes

#define NEW_MUTEX() \
    [false]

#define MUTEX_LOCK(mtx) \
    LOGMSG("TRYING TO LOCK"); \
    waitUntil \
    { \
        ((((mtx) select 0) isEqualTo false) \
            and { (mtx) set [0, true]; true }) \
    }; \
    LOGMSG("LOCKED")

// TODO, explode if we're trying to unlock a mutex that is not locked
#define MUTEX_UNLOCK(mtx) \
    LOGMSG("UNLOCKING"); \
    mtx set [0, false]

#define MUTEX_IS_LOCKED(mtx) \
    (((mtx) select 0) isEqualTo true)

/* Usage for above (todo, place this somewhere else)
[] spawn
{

    g_mutex_array = [NEW_MUTEX(), NEW_MUTEX(), NEW_MUTEX(), NEW_MUTEX()];
    g_values = [0, 0, 0, 0];
    g_runs = [];
    for "_i" from 0 to 19 do
    {
        VAR(_spawn) = _i spawn
            {
                VAR(_idx) = (_this mod 4);
                VAR(_mutex) = g_mutex_array select _idx;
                MUTEX_LOCK(_mutex);
                VAR(_value) = g_values select _idx;
                sleep 0.05;
                g_values set [_idx, _value + 1];
                MUTEX_UNLOCK(_mutex);
            };
        g_runs set [_i, _spawn];
    };
    waitUntil { [{ scriptDone _this }, g_runs] call INEPT_fnc_all };
    assert (g_values isEqualTo [5, 5, 5, 5]);

};
*/
