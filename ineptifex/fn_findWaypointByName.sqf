/* INEPT_fnc_findWaypointByName
 *
 * Parameters:
 *  0: A pawn
 *  1: A string referring to the name of a waypoint
 *
 * Specification:
 *  Looks through the pawn's waypoints and returns the first one that has a
 *  name that matches argument 1.
 */

#define DEBUG_VAR "INEPT_fnc_findWaypointByName_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_pawn) = ARGUMENT_0;
VAR(_name) = ARGUMENT_1;

LOGVAR(_pawn);
LOGVAR(_name);

VAR(_r) = NULL_WP();

{
    if ((waypointName _x) isEqualTo _name) exitWith
    {
        _r = _x;
    };
}
forEach (waypoints (_pawn select PAWN_GROUP));

_r
