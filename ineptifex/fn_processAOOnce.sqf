/* INEPT_fnc_processAOOnce
 *
 * Parameters:
 *  0: An AO instance
 *
 * Specification:
 *  This calls a lot of things, it prunes old contacts, it removes dead pawns,
 *  it finds new targets and correlates old ones, it builds AOIs from the known
 *  baddies, it assigns them to pawns, and then runs INEPT_fnc_processPawnOnce
 *  on each pawn before it returns.
 *  ineptifex will not call this automatically, unless you use
 *  INEPT_fnc_processAOForever or something. So either do that or remember to
 *  call this.
 */

#define DEBUG_VAR "INEPT_fnc_processAOOnce_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;

[_ao] call INEPT_fnc_ageAndPruneContacts;
[_ao] call INEPT_fnc_prunePawns;
[_ao] call INEPT_fnc_rebuildContactIntel;

MUTEX_LOCK(_ao select AO_PAWNS_MUTEX);
VAR(_pawns) = (_ao select AO_PAWNS);

// Delegate to contact AOIs
VAR(_cnt_aois) = [_ao] call INEPT_fnc_contactAOIs;
LOGVAR(_cnt_aois);
_pawns = [_pawns, _cnt_aois] call INEPT_fnc_delegateAOIs;

// Delegate to remaining boring AOIs
MUTEX_LOCK(_ao select AO_AOIS_MUTEX);
// todo, shuffle the _aois so that if some of them have the same firepower we
// are less predictable. xxx, that could be a bad idea, you might end up with
// people flip-flopping around between aois if the same firepower.
VAR(_aois) = _ao select AO_AOIS;
LOGVAR(_aois);
_pawns = [_pawns, _aois] call INEPT_fnc_delegateAOIs;

// TODO put this in its own function?
// Assign unassigned thingies to reserve AOIs
if !(_pawns isEqualTo []) then
{
    // Indexes of AOIs that are marked as being reserve thingies
    VAR(_reserves) = [
        // I can't imagine why a contact AOI would be reseve but just in case
        _cnt_aois + _aois,
        {_x select AOI_IS_RESERVE}
        ] call BIS_fnc_conditionalSelect;
    LOGVAR(_reserves);
    if !(_reserves isEqualTo []) then
    {
        LOGMSG("Some pawns are unassigned and there exists at least one reserve AOI");
        {
            VAR(_pawn) = _x;
            VAR(_pawn_pos) = [_pawn] call INEPT_fnc_getPawnPos;
            // TODO, maybe pick the reserve that is closest?
            VAR(_closest_idx) = [
                _reserves,
                {
                    1 / (1 + (_pawn_pos distance (_x select AOI_POS)))
                }
                ] call INEPT_fnc_indexOfBest;
            VAR(_aoi) = _reserves select _closest_idx;
            if !((_pawn select PAWN_AOI) isEqualTo _aoi) then
            {
                [_pawn, _aoi] call INEPT_fnc_assignAOI;
            };
        }
        forEach _pawns;
    }
    else
    {
        // Finally unassign the remaining pawns
        {
            VAR(_pawn) = _x;
            if !((_pawn select PAWN_AOI) isEqualTo NULL_AOI()) then
            {
                [_pawn, NULL_AOI()] call INEPT_fnc_assignAOI;
            };
        }
        forEach _pawns;
    };
};

MUTEX_UNLOCK(_ao select AO_AOIS_MUTEX);

// I don't think these actually run in parallel, so this is probably not an
// optimization in any way ... but at least it lets the pawn process scripts
// sleep if for some reason they need to
VAR(_pawn_scripts) = [];
{
    VAR(_handle) = [_ao, _x] spawn INEPT_fnc_processPawnOnce;
    _pawn_scripts pushBack _handle;
}
forEach (_ao select AO_PAWNS);
waitUntil
{
    [_pawn_scripts, {scriptDone _x}] call INEPT_fnc_all;
};
MUTEX_UNLOCK(_ao select AO_PAWNS_MUTEX);

LOGTICK();
LOGMSG("w00t!");
