/* INEPT_fnc_magazinesForWeaponCfg
 *
 * Parameters:
 *  0: Class name of CfgWeapons entry
 *
 * Specification:
 *  Returns an array of pairs. First field in the pair is a muzzle class (not a
 *  string) for the given weapon. Second field is an array of CfgMagazine class
 *  names (a string) for that muzzle.
 */

#define DEBUG_VAR "INEPT_fnc_magazinesForWeaponCfg_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_class_name) = ARGUMENT_0;

VAR(_muzzles) = getArray (configFile >> "CfgWeapons" >> _class_name >> "muzzles");
if (_muzzles isEqualTo []) then
{
    ["Apparently %1 has no muzzles, according to its entry in CfgWeapons", _x] call BIS_fnc_error;
    []
}
else
{
    VAR(_r) = [];
    {
        VAR(_muzzle_cfg) = if (_x == "this") then
            {
                (configFile >> "CfgWeapons" >> _class_name)
            }
            else
            {
                (configFile >> "CfgWeapons" >> _class_name >> _x)
            };
        _r pushBack (NEW_PAIR(_muzzle_cfg, getArray (_muzzle_cfg >> "magazines")));
    }
    forEach _muzzles;
    LOGVAR(_r);
    _r
}
