/* INEPT_fnc_addFirepower
 *
 * Parameters:
 *  0: A firepower instance
 *  1: A firepower instance
 *
 * Specification:
 *  Adds two firepower instances and returns the result. The arguments are not
 *  modified. At the moment, this is just a sum, with the exception of the
 *  FIREPOWER_MOBILITY and FIREPOWER_STEALTH fields, which are left as -1.
 */

#define DEBUG_VAR "INEPT_fnc_addFirepower_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_lh) = ARGUMENT_0;
VAR(_rh) = ARGUMENT_1;

// too bad we can't use vectorAdd :/
[
    (_lh select 0) + (_rh select 0),
    (_lh select 1) + (_rh select 1),
    (_lh select 2) + (_rh select 2),
    (_lh select 3) + (_rh select 3),
    (_lh select 4) + (_rh select 4),
    (_lh select 5) + (_rh select 5),
    -1,
    -1
    ]
