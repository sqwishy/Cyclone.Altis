/* INEPT_fnc_getMobility
 *
 * Parameters:
 *  0: A unit info instance (see INEPT_fnc_getUnitInfo)
 *
 * Specification:
 *  Similar to INEPT_fnc_getFirepower, but returns only the value for the
 *  mobility.
 */

#define DEBUG_VAR "INEPT_fnc_getMobility_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_class_name) = ARGUMENT_0;

VAR(_maxSpeed) = getNumber (configFile >> "CfgVehicles" >> _class_name >> "maxSpeed");
_maxSpeed = _maxSpeed * 0.1667; // Convert to 100 metres per minute
switch (true) do
{
    case (_class_name isKindOf "Man"):
    {
        // The vanilla configs say that dudes can run 400m in a minute, I
        // think that's only when you aren't holding a weapon, so its
        // scaled down because it's assuming you're armed and dangerous
        _maxSpeed * 0.6
    };
    case (_class_name isKindOf "Car");
    case (_class_name isKindOf "Tank"):
    {
        // Slower if not on roads, roads are windey
        _maxSpeed * 0.7
    };
    case (_class_name isKindOf "Air"):
    {
        _maxSpeed * 0.9
    };
    default
    {
        _maxSpeed
    };
}
