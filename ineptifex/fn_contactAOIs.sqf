/* INEPT_fnc_contactAOIs
 *
 * Parameters:
 *  0: An AO instance.
 *
 * Specification:
 *  Generates and returns a list of AOIs that represent the known contacts.
 */

#define DEBUG_VAR "INEPT_fnc_contactAOIs_is_verbose"

#include "common\defines.h"
#include "data.h"

#define CONTACT_AOI_RADIUS          100
#define WANDER_LIMIT                300
// How much of the unit's mobility is added to the dispersion (100 metres per
// mobility per minute)
#define WANDER_MULTIPLIER           0.15

LOGENTRY();

VAR(_ao) = ARGUMENT_0;

VAR(_aois) = [];

MUTEX_LOCK(_ao select AO_CONTACTS_MUTEX);

// Shallow copy since we will be modifying this list
VAR(_contacts) = (_ao select AO_CONTACTS) + [];

while {!(_contacts isEqualTo [])} do
{
    VAR(_max_wander) = 0;
    VAR(_uninspected) = [_contacts call BIS_fnc_arrayPop];
    VAR(_group) = [];
    while {!(_uninspected isEqualTo [])} do
    {
        VAR(_current) = _uninspected call BIS_fnc_arrayPop;
        VAR(_current_pos) = _current select CONTACT_POS;

        for [{_x = (count _contacts) - 1}, { _x >= 0 }, { _x = _x - 1 }] do
        {
            VAR(_x_pos) = (_contacts select _x) select CONTACT_POS;
            if ((_current_pos distance _x_pos) < CONTACT_AOI_RADIUS) then
            {
                _uninspected pushBack (_contacts deleteAt _x);
            };
        };

        // Shouldn't this be in INEPT_fnc_aoiFromContacts? The difference
        // between those two functions are confusing me
        VAR(_wander) = WANDER_LIMIT min (
            WANDER_MULTIPLIER
            * 100 * (_current select CONTACT_MOBILITY)
            * ((time - (_current select CONTACT_LASTSEEN)) / 60)
            );
        if (_wander > _max_wander) then
        {
            LOGVAR(_wander);
            _max_wander = _wander;
        };

        _group pushBack _current;
    };
    LOGVAR(_uninspected);
    LOGVAR(_group);
    LOGVAR(_contacts);

    VAR(_aoi) = [_group] call INEPT_fnc_aoiFromContacts;
    _aoi set [AOI_DISPERSION, _max_wander + (_aoi select AOI_DISPERSION)];
    LOGVAR(_aois);
    LOGVAR(_aoi);
    _aois pushBack _aoi;
    LOGVAR(_aois);
};

MUTEX_UNLOCK(_ao select AO_CONTACTS_MUTEX);

LOGVAR(_aois);
_aois
