/* INEPT_fnc_addAOI
 *
 * Parameters:
 *  0: An AO instance
 *  1: An AOI instance
 *
 * Specification:
 *  Adds the AOI to the AO.
 */

#define DEBUG_VAR "INEPT_fnc_addAOI_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;
VAR(_aoi) = ARGUMENT_1;

VAR(_mutex) = _ao select AO_AOIS_MUTEX;
MUTEX_LOCK(_mutex);
VAR(_aois) = _ao select AO_AOIS;
_aois pushBack _aoi;
MUTEX_UNLOCK(_mutex);
