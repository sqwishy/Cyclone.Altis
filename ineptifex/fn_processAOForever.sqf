/* INEPT_fnc_processAOForever
 *
 * Parameters:
 *  0: An AO instance
 *
 * Specification:
 *  Calls INEPT_fnc_processAOOnce on argument 0 forever with eight second
 *  intervals.
 */

#define DEBUG_VAR "INEPT_fnc_processAOForever_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;

while {true} do
{
    [_ao] call INEPT_fnc_processAOOnce;
    sleep 8;
};
