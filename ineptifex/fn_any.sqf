/* INEPT_fnc_any
 *
 * Parameters:
 *  0: An array of whatevers.
 *  1: A callable, returning true or false, where _x will be equal to one of
 *     the elements in the previous argument.
 *
 * Specification:
 *  Returns true if any elements in argument 0 return true when being passed as
 *  an argument to argument 1. Therefore, returns false if argument 0 is an
 *  empty array.
 */

#define DEBUG_VAR "INEPT_fnc_any_is_verbose"

#include "common\defines.h"

VAR(_array) = ARGUMENT_0;
if ([] isEqualTo _array) exitWith {false};
{
    if (call ARGUMENT_1) exitWith {true};
    false
}
forEach _array
