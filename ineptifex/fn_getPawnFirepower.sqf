/* INEPT_fnc_getPawnFirepower
 *
 * Parameters:
 *  0: A pawn instance
 *
 * Specification:
 *  Returns a firepower instance for the given pawn.
 *  Things get a little strange when vehicles are involved, especially when
 *  members of a pawn are in vehicles that it isn't commanding
 */

#define DEBUG_VAR "INEPT_fnc_getPawnFirepower_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_pawn) = ARGUMENT_0;

VAR(_peeps) = units (_pawn select PAWN_GROUP);

LOGVAR(_peeps);

if ([] isEqualTo _peeps) exitWith {NEW_FIREPOWER()};

// A list of vehicles that we "own"
VAR(_vehicles) = [];
VAR(_infos) = [];

{
    // I am assuming that all of the units are people
    if (// Are we in a vehicle
        vehicle _x != _x
        // And in command of it
        and {effectiveCommander vehicle _x == _x}
        ) then
    {
        _vehicles pushBack (vehicle _x);
        _infos pushBack (
            [
                vehicle _x,
                UNIT_INFO_ACCURATE,
                UNIT_INFO_NO_CARGO
            ] call INEPT_fnc_getUnitInfo
            );
    };
}
forEach _peeps;

LOGVAR(_infos);

{
    VAR(_x_info) = [
        _x,
        UNIT_INFO_ACCURATE,
        UNIT_INFO_NO_CARGO
        ] call INEPT_fnc_getUnitInfo;
    if (vehicle _x != _x) then
    {
        // In a vehicle, check if it's one of the ones we own
        VAR(_idx) = _vehicles find (vehicle _x);
        if (_idx == -1) then
        {
            // I guess we're in someone else's car, maybe we're being
            // transported?
            _infos pushBack _x_info;
        }
        else
        {
            VAR(_veh_info) = _infos select _idx;
            (_veh_info select UNIT_INFO_CARGO) pushBack _x_info;
        };
    }
    else
    {
        _infos pushBack _x_info;
    };
}
forEach _peeps;

LOGVAR(_vehicles);
LOGVAR(_infos);

[_infos] call INEPT_fnc_getCombinedFirepower
