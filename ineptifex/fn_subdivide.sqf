/* INEPT_fnc_subdivide
 *
 * Parameters:
 *  0: An area, see data.h, the AREA_* stuff there
 *  1: A scalar defining the radius if the subdivisions made
 *
 * Specification:
 *  Returns a list of 2D positions inside of argument 0. If circles with
 *  argument 1 radius were made at the positions, they would overlap slightly,
 *  but cover most of the given area. And they would be somewhat equally spaced
 *  I think ... for the most part.
 */

#define DEBUG_VAR "INEPT_fnc_subdivide_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

LOGVAR(ARGUMENT);

VAR(_area) = ARGUMENT_0;
VAR(_subdivision_radius) = ARGUMENT_1;

if (_subdivision_radius <= 0) exitWith
{
    ["Subdivision radius must be greater than zero"] call BIS_fnc_error;
    [_area select AREA_POS]
};

if !((_area select AREA_ANGLE) isEqualTo 0) exitWith
{
    ["Given area has a non-zero rotation (%1), my implementer was not smart enough to handle this case", _area select AREA_ANGLE] call BIS_fnc_error;
    [_area select AREA_POS]
};

VAR(_pos) = _area select AREA_POS;
// This is kind of a lie, the width is the radius if this shit is a circle, or
// the half of the length of one of the sides if its a square or rectangle.
// _height has a similar thing going on
VAR(_width) = _area select AREA_WIDTH;
VAR(_height) = _area select AREA_HEIGHT;

if ((_width / _subdivision_radius) > 1e4
    or {(_height / _subdivision_radius) > 1e4}) exitWith
{
    ["I don't think you know what you're doing"] call BIS_fnc_error;
    [_area select AREA_POS]
};

// todo, handle case where requested subdivision is too large for the given
// location

// This is kind of yucky ...
VAR(_loc) = [_area] call INEPT_fnc_locationFromArea;

VAR(_width_stepsize) = 2 * _subdivision_radius;
VAR(_width_steps) = ceil (2 * _width / _width_stepsize);

VAR(_height_stepsize) = _subdivision_radius * (sqrt 2);
VAR(_height_steps) = ceil (2 * _height / _height_stepsize);

VAR(_width_offset) = (_width - (_width_stepsize / 2)) mod _width_stepsize;
VAR(_height_offset) = (_height - (_height_stepsize / 2)) mod (_height_stepsize / 2);
VAR(_start) = [
    (_pos select 0) - _width + _width_offset,
    (_pos select 1) - _height + _height_offset
    ];

LOGVAR(_pos);
LOGVAR(_start);

LOGVAR(_subdivision_radius);
LOGVAR(_width);
LOGVAR(_height);
LOGVAR(_width_stepsize);
LOGVAR(_height_stepsize);
LOGVAR(_width_steps);
LOGVAR(_height_steps);
LOGVAR(_width_offset);
LOGVAR(_height_offset);

VAR(_results) = [];
for "_y" from 0 to _height_steps - 1 do
{
    VAR(_is_staggered_row) = (_y mod 2) == 1;
    VAR(_row_height_thing) = _y * _height_stepsize;
    for "_x" from 0 to _width_steps do
    {
        if (_x != _width_steps or _is_staggered_row) then
        {
            // TODO, this is shifty, _pos is already declared and shit
            VAR(_pos) =
                if (_is_staggered_row) then
                {
                    [
                    (_start select 0) + _x * _width_stepsize - _subdivision_radius,
                    (_start select 1) + _row_height_thing
                    ]
                }
                else
                {
                    [
                    (_start select 0) + _x * _width_stepsize,
                    (_start select 1) + _row_height_thing
                    ]
                };
            if (_pos in _loc) then
            {
                _results pushBack _pos;
            };
        };
    };
};
deleteLocation _loc;
_results
