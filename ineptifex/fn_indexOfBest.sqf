/* INEPT_fnc_indexOfBest
 *
 * Parameters:
 *  0: An array of thingies
 *  1: A callable, returning a scalar, where _x is an element in argument 0
 *
 * Specification:
 *  Calls argument 1 on all elements in argument 0, returns the index for the
 *  element that yielded the greatest value. Returns -1 if argument 0 is empty.
 */

#define DEBUG_VAR "INEPT_fnc_indexOfBest_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

private _list = ARGUMENT_0;

if (_list isEqualTo []) exitWith {-1};
if (1 == count _list) exitWith {0};

private _best_index = 0;
private _x = _list select 0;
private _best_quality = call (ARGUMENT_1);
private __forEachIndex = 0;
while {__forEachIndex = __forEachIndex + 1; __forEachIndex < (count _list)} do
{
    _x = _list select __forEachIndex;
    // Silly hack because of dynamic typing issues where this variable is being
    // clobbered by something else.
    private _forEachIndex = __forEachIndex;
    private _quality = call (ARGUMENT_1);
    if (_quality > _best_quality) then
    {
        _best_index = __forEachIndex;
        _best_quality = _quality;
    };
};
_best_index
