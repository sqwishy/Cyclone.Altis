/* INEPT_fnc_assignToVehicle
 *
 * Parameters:
 *  0: A list of units
 *  1: A vehicle
 *
 * Specification:
 *  Randomly assigns the units in argument 0 to roles in the given vehicle.
 *  Group leaders are assigned last.
 *  Note, this does not mess with the allowGetIn command.
 *  Also note, this might not work properly in conditions when other units are
 *  assigned to positions in this vehicle but not actually in them yet. I'm not
 *  sure if there's a good way to handle that until a future version of Arma
 *  comes out with a few more commands.
 *  Also, this probably doesn't handle multi-turret vehicles very well.
 */

#define DEBUG_VAR "INEPT_fnc_assignToVehicle_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_units) = ARGUMENT_0;
VAR(_tgt) = ARGUMENT_1;

LOGVAR(_units);
LOGVAR(_tgt);

VAR(_leaders) = [_units, {_x == leader group _x}] call BIS_fnc_conditionalSelect;
VAR(_non_leaders) = _units - _leaders;
LOGVAR(_leaders);
LOGVAR(_non_leaders);

{
    if ((_non_leaders isEqualTo []) and {_leaders isEqualTo []}) exitWith {};

    VAR(_pos_name) = _x select 0;
    VAR(_num_slots) = _tgt emptyPositions _pos_name;
    LOGVAR(_pos_name);
    LOGVAR(_num_slots);

    VAR(_continue) = while {_num_slots > 0} do
        {
            VAR(_unit) = if (_non_leaders isEqualTo []) then
                {
                    _leaders call INEPT_fnc_popRandom
                }
                else
                {
                    _non_leaders call INEPT_fnc_popRandom
                };
            LOGVAR(_unit);
            call (_x select 1);
            if ((_non_leaders isEqualTo []) and {_leaders isEqualTo []}) exitWith {false};
            _num_slots = _num_slots - 1;
            true
        };
    // TODO, clean this code up
    if (not isNil "_continue" and {not _continue}) exitWith {};
}
forEach [
    ["driver",      {_unit assignAsDriver _tgt}],
    ["gunner",      {_unit assignAsGunner _tgt}],
    ["commander",   {_unit assignAsCommander _tgt}],
    ["cargo",       {_unit assignAsCargo _tgt}]
    ];
