/* INEPT_fnc_popRandom
 *
 * Parameters:
 *  An array, but the array passed is the argument, not an element in like an
 *  argument array.
 *  For example, do `_my_list call INEPT_fnc_popRandom`, not
 *  `[_my_list] call INEPT_fnc_popRandom` like you would with anything else.
 *  Sorry for the inconsistency, I will commit sudoku.
 *
 * Specification:
 *  Selects and returns an element from an array at random removing the element
 *  from the array in-place. Behaviour is undefined if you pass an empty array.
 */
_this deleteAt (floor random (count _this))
