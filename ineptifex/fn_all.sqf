/* INEPT_fnc_all
 *
 * Parameters:
 *  0: An array of whatevers.
 *  1: A callable, returning true or false, where _x will be equal to one of
 *     the elements in the previous argument.
 *
 * Specification:
 *  Returns true if none of the elements in argument 0 return false when being
 *  passed as an argument to argument 1. Therefore, returns true if argument 0
 *  is an empty array.
 */


#define DEBUG_VAR "INEPT_fnc_all_is_verbose"

#include "common\defines.h"

VAR(_array) = ARGUMENT_0;
if ([] isEqualTo _array) exitWith {true};
{
    if !(call ARGUMENT_1) exitWith {false};
    true
}
forEach _array
