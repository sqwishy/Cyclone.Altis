/* INEPT_fnc_correlateContact
 *
 * Parameters:
 *  0: A list of contacts to query from
 *  1: The class name of the new contact
 *  2: The position of the new contact
 *  3: The time that the new contact was last seen
 *
 * Specification:
 *  Finds a contact in argument 0 that resembles what last three arguments
 *  describe. Returns an index in _contacts that matches best, or -1 if no
 *  correlation was found.
 */

#define DEBUG_VAR "INEPT_fnc_correlateContact_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

// TODO, is there a good reason we don't just take a contact in place of the
// last four args?
params [ "_contacts"
       , "_classname"
       , "_position"
       , "_time"
       , ["_given_mobility", 0] ];

if (LOGGING_ENABLED()) then
{
    LOGVAR(count _contacts);
    {
        LOGVAR(_x select CONTACT_INFO select UNIT_INFO_CLASS);
        LOGVAR(_x select CONTACT_INFO select UNIT_INFO_CARGO);
    }
    forEach _contacts;
    LOGVAR(_classname);
    LOGVAR(_position);
    LOGVAR(_time);
};

VAR(_closest_path) = NEW_PAIR(-1, -1);
VAR(_closest_value) = 0;

{
    VAR(_con) = _x;
    LOGVAR(_con);
    if (
        ((_con select CONTACT_INFO) select UNIT_INFO_CLASS) isEqualTo _classname
        ) then
    {
        VAR(_mobility) = (_con select CONTACT_MOBILITY) max _given_mobility;
        VAR(_dist) = _position distance (_con select CONTACT_POS);
        VAR(_time_elapsed) = _time - (_con select CONTACT_LASTSEEN);
        // For the purposes of correlation, we are liberal with mobility
        // measurements. It's better to make false correlations than to not
        // make a correlation where one ought to be.
        // And sometimes things move faster than we might expect.
        VAR(_r) = _dist - (1.5 * 100 * _mobility * _time_elapsed / 60);
        LOGVAR(_dist);
        LOGVAR(_mobility);
        LOGVAR(_time_elapsed);
        LOGVAR(_r);
        if (_r < _closest_value) then
        {
            _closest_path = NEW_PAIR(_forEachIndex, -1);
            _closest_value = _r;
            LOGVAR(_closest_path);
            LOGVAR(_closest_value);
        };
    }
    else
    {
        VAR(_cargo) = (_con select CONTACT_INFO) select UNIT_INFO_CARGO;
        if !(_cargo isEqualTo []) then
        {
            VAR(_cnt_idx) = _forEachIndex;
            {
                // Holy fucking code dupe, batman
                VAR(_cargo_info) = _x;
                LOGVAR(_cargo_info);
                if (
                    (_cargo_info select UNIT_INFO_CLASS) isEqualTo _classname
                    ) then
                {
                    VAR(_mobility) = (_con select CONTACT_MOBILITY) max _given_mobility;
                    VAR(_dist) = _position distance (_con select CONTACT_POS);
                    VAR(_time_elapsed) = _time - (_con select CONTACT_LASTSEEN);
                    VAR(_r) = _dist - (1.5 * 100 * _mobility * _time_elapsed / 60);
                    LOGVAR(_dist);
                    LOGVAR(_mobility);
                    LOGVAR(_time_elapsed);
                    LOGVAR(_r);
                    if (_r < _closest_value) then
                    {
                        _closest_path = NEW_PAIR(_cnt_idx, _forEachIndex);
                        _closest_value = _r;
                        LOGVAR(_closest_path);
                        LOGVAR(_closest_value);
                    };
                };
            }
            forEach _cargo;
        };
    };
}
forEach _contacts;

_closest_path
