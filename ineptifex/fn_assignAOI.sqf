/* INEPT_fnc_assignAOI
 *
 * Parameters:
 *  0: A pawn instance
 *  1: An AOI instance
 *
 * Specification:
 *  Configures the given pawn to be assigned to the given AOI.
 */

#define DEBUG_VAR "INEPT_fnc_assignAOI_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_pawn) = ARGUMENT_0;
VAR(_new_aoi) = ARGUMENT_1;

LOGVAR(_pawn);

MUTEX_LOCK(_pawn select PAWN_MUTEX);

// Reset the pawn's behaviour state if AOI behaviour changes
VAR(_curr_aoi) = _pawn select PAWN_AOI;

VAR(_curr_logic) = [_curr_aoi] call INEPT_fnc_getBehaviorLogic;
VAR(_new_logic) = [_new_aoi] call INEPT_fnc_getBehaviorLogic;

// TODO, which is faster/better?
//if !(_new_logic isEqualTo _curr_logic)
VAR(_logic_is_changing) = !([_new_logic, _curr_logic] call INEPT_fnc_isSameInstance);
if (_logic_is_changing) then
{
    [_ao, _pawn] call (_curr_logic select BEHAVIOR_LOGIC_UNINSTALL);
    _pawn set [PAWN_BEHAVIOR_STATE, NULL_BEHAVIOR_STATE()];
};

_pawn set [PAWN_AOI, _new_aoi];

if (_logic_is_changing) then
{
    [_ao, _pawn] call (_new_logic select BEHAVIOR_LOGIC_INSTALL);
};

MUTEX_UNLOCK(_pawn select PAWN_MUTEX);
