/* INEPT_fnc_selectFractionAtRandom
 *
 * Parameters:
 *  0: A list of thingies
 *  1: A scalar from 0 to 1
 *
 * Specification:
 *  Randomly selects argument 1 percent of elements (rounded up) from argument
 *  1 and retuns a list containing only those selected.
 */

#define DEBUG_VAR "INEPT_fnc_selectFractionAtRandom_is_verbose"

#include "common\defines.h"

LOGENTRY();

VAR(_list) = (ARGUMENT_0) + [];
[_list, ARGUMENT_1] call INEPT_fnc_selectFractionAtRandomIP;
_list
