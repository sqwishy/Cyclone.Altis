/* INEPT_fnc_newAOFromEditor
 *
 * Parameters:
 *  0: A game logic
 *  1: A side
 *
 * Specification:
 *  Similar to INEPT_fnc_newAO, but this will look for objects synchronized to
 *  argument 0 and add them as pawns. This only supports synchronizing groups to
 *  the AO. AOIs have to added by hand. The synchs will be removed after the
 *  groups are added. Returns the new AO.
 */

#define DEBUG_VAR "INEPT_fnc_newAOFromEditor_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_handle) = ARGUMENT_0;
VAR(_side) = ARGUMENT_1;
VAR(_ao) = [_side] call INEPT_fnc_newAO;
{
    [_ao, group _x] call INEPT_fnc_addGroup;
}
forEach (synchronizedObjects _handle);
_handle synchronizeObjectsRemove (synchronizedObjects _handle);
_ao
