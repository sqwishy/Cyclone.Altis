// See INEPT_fnc_buildBehaviorLogics

#define DEBUG_VAR "INEPT_fnc_behaviorAssaultUninstall_is_verbose"

#include "common\defines.h"
#include "data.h"
#include "behaviordata.h"

LOGENTRY();

params ["_ao", "_pawn"];
private _wp = [_pawn, ASSAULT_WP_NAME] call INEPT_fnc_findWaypointByName;
if !(_wp isEqualTo NULL_WP()) then
{
    deleteWaypoint _wp;
};
_wp = [_pawn, FLANK_WP_NAME] call INEPT_fnc_findWaypointByName;
if !(_wp isEqualTo NULL_WP()) then
{
    deleteWaypoint _wp;
};
