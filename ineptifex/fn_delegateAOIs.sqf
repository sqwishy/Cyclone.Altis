/* INEPT_fnc_delegateAOIs
 *
 * Parameters:
 *  0: List of pawns
 *  1: List of AOIs
 *
 * Specification:
 *  Assigns the given AOIs to the given pawns. This returns a list of first
 *  pawns from argument 0 that were not assigned. That includes pawns that were
 *  assigned but lost their assignment when this function was called.
 *  This is called by INEPT_fnc_processAOOnce, so you don't normally need to
 *  call this.
 */


// This will go through the AOIs and assign them to pawns

#define DEBUG_VAR "INEPT_fnc_delegateAOIs_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_pawns) = ARGUMENT_0;
VAR(_aois) = ARGUMENT_1;

if ((_pawns isEqualTo []) or {_aois isEqualTo []}) exitWith
{
    _pawns
};

LOGVAR(_pawns);
LOGVAR(_aois);

// For every element in _assignments, the index of that element in _assignments
// is equal to the index of the pawn in _pawns that the element is for and the
// value of that element is equal to the index of the poi in _aois assigned to
// the pawn
VAR(_assignments) = [];
// List of indexes of _pawns that haven't been assigned anything by this pass
VAR(_unassigned) = [];
{
    _assignments pushBack -1;
    _unassigned pushBack _forEachIndex;
}
forEach _pawns;

LOGVAR(_assignments);
LOGVAR(_unassigned);

// Sum of firepower assigned to aois
VAR(_assigned_firepower) = [];
VAR(_aoi_threats) = [];
{
    VAR(_fp) = _x select AOI_FIREPOWER;
    VAR(_threat) = [_fp] call INEPT_fnc_getThreat;
    _aoi_threats pushBack _threat;
    _assigned_firepower pushBack NEW_FIREPOWER();
}
forEach _aois;

LOGVAR(_aoi_threats);

// Array of pawn firepowers - lazily initilized
VAR(_pawn_fps) = [];

// Loop through aois and assign pawns to them
while {true} do
{
    if (_aoi_threats isEqualTo []) exitWith
    {
        LOGMSG("No threats, exiting ...");
    };

    VAR(_scariest_aoi_index) = [
        _aoi_threats,
        {_x}
        ] call INEPT_fnc_indexOfBest;
    LOGVAR(_scariest_aoi_index);

    VAR(_aoi) = _aois select _scariest_aoi_index;
    VAR(_aoi_fp) = _aoi select AOI_FIREPOWER;
    VAR(_anti_fp) = NEW_FIREPOWER();
    LOGMSG("Found scarriest AOI:");
    LOGVAR(_aoi);
    LOGVAR(_aoi_fp);
    VAR(_assigned) = _assigned_firepower select _scariest_aoi_index;
    LOGVAR(_assigned);
    VAR(_scariest_threat) = _aoi_threats select _scariest_aoi_index;
    if (_scariest_threat <= 0) exitWith
    {
        LOGMSG("No remaining AOIs of any threat, exiting");
    };

    // Lazy initilization
    // todo, beware of dead pawns
    if (_pawn_fps isEqualTo []) then
    {
        {
            VAR(_fp) = [_x] call INEPT_fnc_getPawnFirepower;
            _pawn_fps pushBack _fp;
        }
        forEach _pawns;
        LOGMSG("Calculating pawn firepowers");
        LOGVAR(_pawn_fps);
    };

    // Figure out who would be the most effective against our scary poi
    LOGTICK();
    VAR(_best_index) = [
        _unassigned,
        {
            VAR(_pawn) = _pawns select _x;
            LOGMSG("Determining pawn effectiveness:");
            LOGVAR(_pawn);
            VAR(_pawn_fp) = _pawn_fps select _x;
            LOGVAR(_pawn_fp);
            VAR(_effective_pawn_fp) = [_assigned, _pawn_fp] call INEPT_fnc_addFirepower;
            LOGVAR(_effective_pawn_fp);
            VAR(_effect) = [_aoi_fp, _effective_pawn_fp] call INEPT_fnc_getEffectiveness;
            LOGVAR(_effect);
            VAR(_dist) = ((leader (_pawn select PAWN_GROUP))
                          distance
                          (_aoi select AOI_POS));
            LOGVAR(_dist);
            VAR(_mobility_modifier) = 100 * (_pawn_fp select FIREPOWER_MOBILITY);
            assert (_mobility_modifier >= 0);
            LOGVAR(_mobility_modifier);
            // This effects the decision making a lot. By tweaking
            // _dist_penalty, closer pawns will be considered before pawns that
            // are further or slower
            VAR(_dist_penalty) = if (_mobility_modifier == 0) then
                {
                    0.5
                }
                else
                {
                    if (_dist <= 0) then
                    {
                        10
                    }
                    else
                    {
                        10 min (0.5 / ((_dist / _mobility_modifier) ^ 0.5) + 0.5)
                    }
                };
            LOGVAR(_dist_penalty);
            _effect = _dist_penalty * _effect;
            LOGVAR(_effect);
            _effect
        }
        ] call INEPT_fnc_indexOfBest;
    LOGTICK();

    // Remove _best_index from _unassigned so we don't select it in the future
    VAR(_best_pawn_index) = _unassigned deleteAt _best_index;

    // Assign the pawn that would prove most effective to go after the threat
    // we are dealing with
    _assignments set [_best_pawn_index, _scariest_aoi_index];

    if (_unassigned isEqualTo []) exitWith {};

    VAR(_best_pawn_fps) = _pawn_fps select _best_pawn_index;

    VAR(_assigned) = [_assigned, _best_pawn_fps] call INEPT_fnc_addFirepower;
    LOGVAR(_assigned);
    _assigned_firepower set [_scariest_aoi_index, _assigned];
    LOGVAR(_assigned_firepower);

    VAR(_fp) = [_aoi_fp, _assigned] call INEPT_fnc_reduceFirepower;
    VAR(_threat) = [_fp] call INEPT_fnc_getThreat;
    LOGMSG("Reducing firepower of AOI and determining new threat");
    LOGVAR(_fp);
    LOGVAR(_threat);
    _aoi_threats set [_scariest_aoi_index, _threat];
    LOGVAR(_aoi_threats);
};

LOGVAR(_unassigned);
LOGVAR(_assignments);

// Return unassigned pawns
VAR(_r) = [];

// Inform the pawns of changes to their assignments - only 
{
    VAR(_pawn) = _pawns select _forEachIndex;
    if (_x == -1) then
    {
        _r pushBack _pawn;
    }
    else
    {
        VAR(_assigned_aoi) = _aois select _x;
        LOGVAR(_pawn);
        LOGVAR(_pawn select PAWN_AOI);
        LOGVAR(_assigned_aoi);
        if !((_pawn select PAWN_AOI) isEqualTo _assigned_aoi) then
        {
            [_pawn, _assigned_aoi] call INEPT_fnc_assignAOI;
            LOGVAR(_pawn select PAWN_AOI);
        };
    };
}
forEach _assignments;

_r
