#define DEBUG_VAR "INEPT_fnc_subdivide_is_verbose"

#include "common\defines.h"
#include "data.h"

VAR(_area) = ARGUMENT_0;
VAR(_loc) = createLocation [ "Strategic"
                           , _area select AREA_POS
                           , _area select AREA_WIDTH
                           , _area select AREA_HEIGHT ];
_loc setRectangular (_area select AREA_ISRECT);
_loc
