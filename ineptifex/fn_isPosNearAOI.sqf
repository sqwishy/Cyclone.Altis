/* INEPT_fnc_isPosNearAOI
 *
 * Parameters:
 *  0: An AOI instance
 *  1: A position, 2D or whatever
 *  2: A distance that defines "near"
 *
 * Specification:
 *  Ignores the z axis of the passed position, argument 1.
 *  Argument 2 is the maximum distance from the edge of the AOI, it can be
 *  positive or negative or zero.
 */

#define DEBUG_VAR "INEPT_fnc_isPosNearAOI_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_aoi) = ARGUMENT_0;
VAR(_pos) = ARGUMENT_1;
VAR(_max_dist) = ARGUMENT_2;

VAR(_aoi_pos) = _aoi select AOI_POS;
_pos = [_pos select 0, _pos select 1, _aoi_pos select 2];

LOGVAR(_aoi_pos);
LOGVAR(_pos);
LOGVAR(_max_dist);

if (
    _max_dist > 0
    and {(_pos distance _aoi_pos) <= (_max_dist)}
    ) exitWith
{
    LOGMSG("Is near, since _pos is fewer than _max_dist units from _aoi_pos");
    true
};

if (_max_dist != 0) then
{
    VAR(_dir) = _pos vectorFromTo _aoi_pos;
    _pos = _pos vectorAdd (_dir vectorMultiply _max_dist);
    LOGVAR(_pos);
};

[_aoi, _pos] call INEPT_fnc_AOIContainsPos
