#include "common\defines.h"
#include "data.h"
#include "behaviordata.h"

params ["_pawn"];
private _grp = _pawn select PAWN_GROUP;
private _wps = waypoints _grp;
if ((currentWaypoint _grp) >= (count _wps)) then
{
    [_pawn] call INEPT_fnc_getPawnPos
}
else
{
    getWPPos (_wps select (currentWaypoint _grp))
}
