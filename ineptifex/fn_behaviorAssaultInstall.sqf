// See INEPT_fnc_buildBehaviorLogics

#define DEBUG_VAR "INEPT_fnc_behaviorAssaultInstall_is_verbose"

#include "common\defines.h"
#include "data.h"
#include "behaviordata.h"

LOGENTRY();

params ["_ao", "_pawn"];
private _grp = _pawn select PAWN_GROUP;
(leader _grp) setBehaviour "AWARE";
