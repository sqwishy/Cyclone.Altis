/* INEPT_fnc_processPawnOnce
 *
 * Parameters:
 *  0: An AO instance
 *  1: A pawn instance
 *
 * Specification:
 *  Does pawn processing stuff, called by INEPT_fnc_processAOOnce, so you
 *  shouldn't have to call this directly.
 */

#define DEBUG_VAR "INEPT_fnc_processPawnOnce_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;
VAR(_pawn) = ARGUMENT_1;
VAR(_grp) = _pawn select PAWN_GROUP;

LOGVAR(_pawn);

if ([units _grp, {not alive _x}] call INEPT_fnc_all) exitWith {};

MUTEX_LOCK(_pawn select PAWN_MUTEX);

[_ao, _pawn] call INEPT_fnc_checkForDeadFolk;

// TODO, somehow figure out a good way to negotiate who gets what if the same
// group tries to steal the same thing. Try to avoid assuming that all parties
// involved are part of the ineptifex AO. It may be the case that a group is
// being controlled by zeus or something.
// However, it's kind of hilarious to watch two groups try to get into the same
// car, and then one group gets in first and just drives off with half of the
// other group ...
if ([units _grp, {isNull assignedVehicle _x}] call INEPT_fnc_all) then
{
    [_pawn] call INEPT_fnc_stealNearbyVehicle;
};

// Ensure that we are in our moving to our assigned AOI and stuff
VAR(_aoi) = _pawn select PAWN_AOI;
LOGVAR(_aoi);
VAR(_logic) = [_aoi] call INEPT_fnc_getBehaviorLogic;
if (isNil "_logic") then {LOGMSG("fuck...")};
[_ao, _pawn] call (_logic select BEHAVIOR_LOGIC_TICK);

MUTEX_UNLOCK(_pawn select PAWN_MUTEX);
