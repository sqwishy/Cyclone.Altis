/* INEPT_fnc_garrisonUnits
 *
 * Parameters:
 *  0: List of units to be garrisoned
 *  1: Building to garrison in
 *
 * Specification:
 *  Orders the given units to move to positions in the given building.
 *  Modifies argument 0 in place, units that are ordered to move into a
 *  building position are removed from the list. Elements remaining in the list
 *  after the function has completed were not ordered to move.
 */

#define DEBUG_VAR "INEPT_fnc_garrisonUnits_is_verbose"

#include "common\defines.h"

LOGENTRY();

VAR(_units) = ARGUMENT_0;
VAR(_building) = ARGUMENT_1;
LOGVAR(_units);
LOGVAR(_building);

VAR(_positions) = _building call BIS_fnc_buildingPositions;

while {
    !(_positions isEqualTo [])
    and {!(_units isEqualTo [])}
    } do
{
    VAR(_u) = _units call INEPT_fnc_popRandom;
    VAR(_pos) = _positions call INEPT_fnc_popRandom;
    _u commandMove _pos;
};
