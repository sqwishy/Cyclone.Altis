/* INEPT_fnc_addGroup
 *
 * Parameters:
 *  0: An AO instance
 *  1: A group
 *
 * Specification:
 *  Creates a new pawn instance for the given group, adds it to the AO, and
 *  returns it.
 *  The units in the group should be local to the machine that this is running
 *  on.
 *  todo, this function should probably check and yell at the user if the above
 *  is violated.
 *  I'm not sure that returning the pawn is a very good idea, I think it's sort
 *  of considered internal data maybe.
 */

#define DEBUG_VAR "INEPT_fnc_addGroup_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();
VAR(_ao) = ARGUMENT_0;
VAR(_grp) = ARGUMENT_1;

VAR(_mtx) = _ao select AO_PAWNS_MUTEX;
MUTEX_LOCK(_mtx);

VAR(_pawn) = NEW_PAWN(_grp);

VAR(_pawns) = _ao select AO_PAWNS;
_pawns pushBack _pawn;

MUTEX_UNLOCK(_mtx);

_pawn
