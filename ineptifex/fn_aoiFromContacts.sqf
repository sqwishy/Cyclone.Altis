/* INEPT_fnc_aoiFromContacts
 *
 * Parameters:
 *  0: Array of contact objects
 *
 * Specification:
 *  Returns an appropriate AOI instance with an area that encloses the contacts,
 */

#define DEBUG_VAR "INEPT_fnc_aoiFromContacts_is_verbose"

#include "common\defines.h"
#include "data.h"

#define MIN_CONTACT_AOI_DISPERSION  25

VAR(_infos) = [];
VAR(_contacts) = ARGUMENT_0;
LOGVAR(_contacts);
LOGVAR(count _contacts);
if (_contacts isEqualTo []) exitWith {NULL_AOI()};

// TODO, what about an overflow?
VAR(_avg) = [0, 0, 0];
{
    _avg = _avg vectorAdd (_x select CONTACT_POS);
    _infos pushBack (_x select CONTACT_INFO);
    LOGVAR(_avg);
}
forEach _contacts;
_avg = _avg vectorMultiply (1 / (count _contacts));
LOGVAR(_avg);

VAR(_dispersion) = MIN_CONTACT_AOI_DISPERSION;
{
    VAR(_dist) = (_x select CONTACT_POS)
                 distance
                 (_avg);
    LOGVAR(_avg);
    LOGVAR(_dist);
    if (_dist > _dispersion) then
    {
        _dispersion = _dist;
    };
}
forEach _contacts;

VAR(_aoi) = NEW_AOI(_avg, _dispersion, BEHAVIOR_ASSAULT, false);
_aoi set [
    AOI_FIREPOWER,
    [_infos] call INEPT_fnc_getCombinedFirepower
    ];
_aoi
