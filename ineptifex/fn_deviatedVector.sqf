/* INEPT_fnc_deviatedVector
 *
 * Parameters:
 *  0: 2d/3d vector
 *  1: Distance of deviation
 *
 * Specification:
 *  Returns a copy of argument 0 translated argument 1 metres away in a random
 *  direction about the z-axis.
 */

#define DEBUG_VAR "INEPT_fnc_deviatedVector_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_vec) = (ARGUMENT_0) + [];
VAR(_delta) = [[ARGUMENT_1, 0], random 360] call INEPT_fnc_rotatedVector;
_vec set [0, (_vec select 0) + (_delta select 0)];
_vec set [1, (_vec select 1) + (_delta select 1)];
_vec
