/* INEPT_fnc_hasVision
 *
 * Parameters:
 *  0: An ASL position
 *
 * Returns:
 *  Number of pawns that are moving to a place where they will have vision on
 *  the given position.
 */

//#define DEBUG_VAR "INEPT_fnc_hasVision_is_verbose"

#define ELEVATION   2

#include "common\defines.h"
#include "data.h"

params ["_ao", "_to_pos", "_omit_pawn"];

_to_pos = _to_pos vectorAdd [0, 0, ELEVATION];

private _count = 0;
private _pawns = (_ao select AO_PAWNS) + [];
if !(isNil "_omit_pawn") then
{
    [_pawns, {!([_x, _omit_pawn] call INEPT_fnc_isSameInstance)}]
        call INEPT_fnc_conditionalSelectIP;
};
private _pawn_dests_asl =
    [_pawns, {ATLtoASL ([_x] call INEPT_fnc_currentPawnDestination)}]
        call INEPT_fnc_applyToAll;
{
    // TODO
    // It would be smart if this added a value > 0 and < 1 depending on how
    // visible surrounding terrain is. But that would get very expensive.
    private _from_pos = _x vectorAdd [0, 0, ELEVATION];
    if (((_from_pos distance _to_pos) < 100) or {!(terrainIntersectASL [_from_pos, _to_pos])}) then
    {
        _count = _count + 1;
    };
}
forEach _pawn_dests_asl;
_count
