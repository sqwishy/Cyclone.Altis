#include "common\defines.h"

params ["_vectors"];

VAR(_r) = [0, 0, 0];
{
    VAR(_v) = _x vectorMultiply (1 / (count _vectors));
    _r = _r vectorAdd _v;
}
forEach _vectors;
_r
