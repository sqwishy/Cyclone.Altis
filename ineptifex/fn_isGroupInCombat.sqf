/* INEPT_fnc_isGroupInCombat
 */

#define DEBUG_VAR "INEPT_fnc_isGroupInCombat_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_grp) = ARGUMENT_0;
LOGVAR(behaviour leader _grp);
(behaviour leader _grp == "COMBAT")
