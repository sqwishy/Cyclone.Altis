/* INEPT_fnc_buildBehaviorLogics
 *
 * Specification:
 *  Behaviours are ways of trying to encapsulate logic for pawns that dictate
 *  how they deal with their AOIs. ineptifex should probably not add or remove
 *  or modify waypoints to pawns unless it is the behaviour that is doing it.
 *  AOIs have a behaviour field that is used by INEPT_fnc_getBehaviorLogic to
 *  return one of the behaviour logics below.
 *  When a logic is being switched out, the uninstall function will be called
 *  on the current logic. This allows the waypoints that that behaviour logic
 *  made to be removed or any other sort of cleanup to happen. Then the install
 *  function on the new behaviuor logic is called on.
 *  All of these functions take one argument, the pawn that the logic is for.
 */

#define DEBUG_VAR "INEPT_fnc_buildBehaviorLogics_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

INEPT_behaviorAssault = NEW_BEHAVIOR_LOGIC(
    INEPT_fnc_behaviorAssaultInstall,
    INEPT_fnc_behaviorAssaultTick,
    INEPT_fnc_behaviorAssaultUninstall
    );

INEPT_behaviorDefend = NEW_BEHAVIOR_LOGIC(
    INEPT_fnc_behaviorDefendInstall,
    INEPT_fnc_behaviorDefendTick,
    INEPT_fnc_behaviorDefendUninstall
    );

INEPT_behaviorNull = NEW_BEHAVIOR_LOGIC(
    INEPT_fnc_behaviorNullInstall,
    INEPT_fnc_behaviorNullTick,
    INEPT_fnc_behaviorNullUninstall
    );
