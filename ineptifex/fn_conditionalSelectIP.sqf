/* INEPT_fnc_conditionalSelectIP
 *
 * Parameters:
 *  0: Array of thingies
 *  1: Callable, returning true or false, where _x is equal to an element from
 *     argument 0.
 *
 * Specification:
 *  Similar to BIS_fnc_conditionalSelect, but operates in-place, so that
 *  argument 0 is modified.
 */

#define DEBUG_VAR "INEPT_fnc_conditionalSelectIP_is_verbose"

#include "common\defines.h"

VAR(_list) = ARGUMENT_0;
for [
    {_forEachIndex = (count _list - 1)},
    {_forEachIndex >= 0},
    {_forEachIndex = _forEachIndex - 1}
    ] do
{
    VAR(_x) = _list select _forEachIndex;
    if !(call (ARGUMENT_1)) then
    {
        _list deleteAt _forEachIndex;
    };
};
_list
