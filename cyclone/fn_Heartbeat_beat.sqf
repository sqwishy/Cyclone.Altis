#include "common\defines.h"

params ["_hb"];

// TODO race condition?
if ([_hb] call cy_fnc_Heartbeat_isDead) then
{
    ["Failing to beat an expired heartbeat. (%1)", _fnc_scriptNameParent] call BIS_fnc_error;
    false
}
else
{
    _hb set [cy_data_Heartbeat_lastBeat, time];
    true
}
