// Meant to run on only one machine on the network ... so probably the server.
// Arguments
//  _cyclone:
//      Cyclone object. (cy_data_Cyclone)
//  _audiance_var:
//      Variable name of Audiance object (cy_data_Audiance).

#include "common\defines.h"

params ["_cyclone", "_audiance_var"];
[ _cyclone                     // cy_data_Narrator_cyclone
, _audiance_var                // cy_data_Narrator_audiance_var
, [0] call cy_fnc_Counter_init // cy_data_Narrator_counter
, NEW_MUTEX()                  // cy_data_Narrator_plots_mutex
, []                           // cy_data_Narrator_plots
, []                           // cy_data_Narrator_gc_queue
] call cy_data_Narrator_init
