#define DEBUG_VAR "cy_fnc_PressureWasher_player_is_verbose"

#include "..\common\defines.h"

#define ROOT_TASK_DESC \
 "Hostile forces have combined to form a notable presence in and around the\
 area %1 (grid %2). In order to inhibit their ability to\
 oragnize, we must disable the logistics node they have operating nearby.<br>\
 <br>\
 To conceal the logistics node, they may have deployed one or more\
 scramblers in the surrounding region. Finding and deactivating\
 scramblers will help us to refine the exact location of the logistics\
 node."

#define CM_TASK_DESC \
 "Destroy or deactivate the scrambler near grid %1 in order to more\
 reliably determine the location of the logistics node operating in the area."

params ["_thrall", "_plot"];

private _plot_id = _plot select cy_data_Plot_id;

private _plotlet  = [_thrall, _plot] call cy_fnc_Thrall_waitForPlotData;

// Using params here seems attractive, but I don't like that it avoids this
// "attribute access" thing I'm trying to do. One of the points of this is that
// it makes a paper-trail in the source code, using params would defeat that
// unless you littered it with comments. Which don't have the advantage of
// failing when they contain a typo.
var(_side)            = _plotlet select cy_data_PressureWasher_side;
var(_terminal)        = _plotlet select cy_data_PressureWasher_terminal;
var(_cms)             = _plotlet select cy_data_PressureWasher_cms;
var(_desc_pos)        = _plotlet select cy_data_PressureWasher_desc_pos;
var(_root_task_pos)   = _plotlet select cy_data_PressureWasher_terminal_task_pos;
var(_cm_tasks_pos)    = _plotlet select cy_data_PressureWasher_cm_tasks_pos;
var(_dangerzone_area) = _plotlet select cy_data_PressureWasher_dangerzone_area;

// HACK HACK HACK
// This is awkward, we modify the plotlet because we isPosInUse needs a
// location and locations cannot be serialized. So we have to initilize it on
// the receiver instead of on the player I guess.
var(_dangerzone) = [_dangerzone_area] call INEPT_fnc_locationFromArea;
_plotlet set [cy_data_PressureWasher_dangerzone, _dangerzone];

var(_interaction_threads) = [];
_interaction_threads pushBack ([_terminal, 8] spawn cy_fnc_PressureWasher_setupInteraction);

var(_task_space) =
    [ _plot_id call cy_fnc_toBase36
    , "tsk"
    ] call cy_fnc_reserveNamespace;

var(_root_task) = ["root" , _task_space] call cy_fnc_reserveNamespace;
var(_cm_tasks) =
    [ _cms
    , {
        [ [_forEachIndex call cy_fnc_toBase36, _root_task] call cy_fnc_reserveNamespace
        , _root_task ]
      }
    ] call INEPT_fnc_applyToAll;

LOGVAR(_root_task);
LOGVAR(_cm_tasks);

var(_active_cms_idx) = [];

[ _root_task
, player
, [ format [ROOT_TASK_DESC, _desc_pos call BIS_fnc_locationDescription, mapGridPosition _desc_pos]
  , "Secure Node"
  , "" ]
, []
, "AUTOASSIGNED"
, nil      // Priority
, true     // Show notifications if true
, false    // Global if true else local
, "Attack" // CfgTaskTypes
, true     // disabled shared objective counter if false else enabled it
] call BIS_fnc_setTask;

{
    var(_cm) = _cms select _forEachIndex;
    var(_is_active) = !([_cm] call cy_fnc_PressureWasher_isDeactivated);
    [ _x
    , player
    , [ format [CM_TASK_DESC, mapGridPosition (_cm_tasks_pos select _forEachIndex)]
        , format ["Scrambler %1", [_forEachIndex + 1] call BIS_fnc_phoneticalWord]
        , "" ]
    , (_cm_tasks_pos select _forEachIndex)
    , if (_is_active) then { "CREATED" } else { "SUCCEEDED" }
    , nil   // Priority
    , false // Show notifications if true
    , false // Global if true else local
    , [_forEachIndex + 1, true] call BIS_fnc_phoneticalWord // CfgTaskTypes
    , true  // disabled shared objective counter if false else enabled it
    ] call BIS_fnc_setTask;
    if (_is_active) then
    {
        _interaction_threads pushBack ([_cm, 8] spawn cy_fnc_PressureWasher_setupInteraction);
        _active_cms_idx pushBack _forEachIndex;
    };
}
forEach _cm_tasks;

if (_active_cms_idx isEqualTo []) then
{
    // Reveal position of pressure washer task
    [_root_task, nil, nil, _root_task_pos, nil, nil, false /* No notification */, false /* Local */] call BIS_fnc_setTask;
};

if (cy_param_DisplayConflictMarkers) then
{
    var(_danger_marker) = format ["PWDangerZone_%1", _plot_id call cy_fnc_toBase36];
    createMarkerLocal [_danger_marker, position _dangerzone];
    _danger_marker setMarkerShapeLocal "ELLIPSE";
    _danger_marker setMarkerSizeLocal (size _dangerzone);
    _danger_marker setMarkerColorLocal "ColorOPFOR";
    _danger_marker setMarkerBrushLocal "DiagGrid";
    _danger_marker setMarkerAlphaLocal 0.3;
};

var(_done) = false;
while
{
    sleep 1;
    (not _done)
    and {not ([_plot] call cy_fnc_Plot_isCancelled)}
}
do
{
    LOGVAR(alive _terminal);
    if ([_terminal] call cy_fnc_PressureWasher_isDeactivated) then
    {
        [_root_task, nil, nil, nil, "SUCCEEDED", nil, false /* No notification */, false /* Local */] call BIS_fnc_setTask;
        [ "TaskSucceeded"
        , ["", "A Logistics Node has been deactivated!"]
        ] call BIS_fnc_showNotification;
        _done = true;
    }
    else
    {
        var(_deactivated) = [];
        LOGVAR(_deactivated);
        {
            var(_cm) = _cms select _x;
            if ([_cm] call cy_fnc_PressureWasher_isDeactivated) then
            {
                _deactivated pushBack _x;

                [_cm_tasks select _x, nil, nil, nil, "SUCCEEDED", nil, false /* No notification */, false /* Local */] call BIS_fnc_setTask;

                [ "TaskSucceeded"
                , ["", "A scrambler has been deactivated."]
                ] call BIS_fnc_showNotification;

                if (count _deactivated == count _active_cms_idx) then
                {
                    // We've run out of scramblers, reveal the pressure washer!
                    [_root_task, nil, nil, _root_task_pos, nil, nil, false /* No notification */, false /* Local */] call BIS_fnc_setTask;

                    [ "TaskUpdated"
                    , ["", format ["A Logistics Node has been detected near grid %1.", mapGridPosition _root_task_pos]]
                    ] call BIS_fnc_showNotification;
                };
            };
        }
        forEach _active_cms_idx;
        _active_cms_idx = _active_cms_idx - _deactivated;
    };
};

if (not _done
    and {[_plot] call cy_fnc_Plot_isCancelled}) then
{
    ["TaskCanceled", ["", "..."]] call BIS_fnc_showNotification;
    {
        private _task = player getVariable (_x call BIS_fnc_taskVar);
        if !((taskState _task) in ["SUCCEEDED", "FAILED"]) then
        {
            _task setTaskState "CANCELED";
        };
    }
    forEach ([_root_task] + _cm_tasks);
};

[_root_task, _cm_tasks] spawn
{
    params ["_root_task", "_cm_tasks"];
    sleep 30;
    [_root_task, player, false] call BIS_fnc_deleteTask;
    {
        [_x select 0, player, false] call BIS_fnc_deleteTask
    }
    forEach _cm_tasks;
};

deleteLocation _dangerzone;;
if (cy_param_DisplayConflictMarkers) then
{
    deleteMarkerLocal _danger_marker;
};
{terminate _x} forEach _interaction_threads;
