params ["_target"];
(alive player) &&
// FIXME, this number is arbitrary and inflexible
// Measure from the base of each thing because _target may be quite tall.
{((getPosASL _target) distance (getPosASL player)) < 6} &&
// Has los-ish roughly I guess.
{
    (not (lineIntersects [eyePos player, getPosWorld _target, _target])) ||
    {not (lineIntersects [eyePos player, getPosASL _target, _target])}
} &&
{not ([_target] call cy_fnc_PressureWasher_isDeactivated)}
