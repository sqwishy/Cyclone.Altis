params ["_narrator"];

private _audiance_var = _narrator select cy_data_Narrator_audiance_var;
private "_audiance";
waitUntil
{
    _audiance = missionNamespace getVariable _audiance_var;
    (not isNil "_audiance") or {sleep 0.1; false}
};
// I feel like I should be shot for this ...
[ _audiance select cy_data_Audiance_last_value
, {missionNamespace getVariable _x}
] call inept_fnc_applyToAll
