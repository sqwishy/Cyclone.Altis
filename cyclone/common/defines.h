#define var(NAME) private #NAME;\
                    NAME

#define inc(EXP) EXP = EXP + 1

#define SERVER_CID (if (isMultiplayer) then {2} else {0})

#define REMOTE_EXEC_EVERYWHERE 0

// Argument handling

#define argument_count (count _this)

#define argument _this
#define argument_n(IDX) (param [IDX])
#define argument_0 argument_n(0)
#define argument_1 argument_n(1)
#define argument_2 argument_n(2)
#define argument_3 argument_n(3)
#define argument_4 argument_n(4)
#define argument_5 argument_n(5)
#define argument_6 argument_n(6)
#define argument_7 argument_n(7)
#define argument_8 argument_n(8)
#define argument_9 argument_n(9)

#define opt_argument_n(DEFAULT, N) \
    if (count _this > N) then { _this select N } else { DEFAULT }
#define opt_argument_0(DEFAULT) opt_argument_n(DEFAULT, 0);
#define opt_argument_1(DEFAULT) opt_argument_n(DEFAULT, 1);
#define opt_argument_2(DEFAULT) opt_argument_n(DEFAULT, 2);
#define opt_argument_3(DEFAULT) opt_argument_n(DEFAULT, 3);
#define opt_argument_4(DEFAULT) opt_argument_n(DEFAULT, 4);
#define opt_argument_5(DEFAULT) opt_argument_n(DEFAULT, 5);
#define opt_argument_6(DEFAULT) opt_argument_n(DEFAULT, 6);
#define opt_argument_7(DEFAULT) opt_argument_n(DEFAULT, 7);
#define opt_argument_8(DEFAULT) opt_argument_n(DEFAULT, 8);
#define opt_argument_9(DEFAULT) opt_argument_n(DEFAULT, 9);

// Logging

// N.B.: _fnc_scriptName is unreliable
#define WHEREAMI() \
    format [ \
        "%1:%2", \
        if (isNil "_fnc_scriptName") then { __FILE__ } else { _fnc_scriptName }, \
        __LINE__ + 1 \
        ]

#ifdef DEBUG_VAR
    #define LOGGING_ENABLED() (!isNil DEBUG_VAR)
#else
    #define LOGGING_ENABLED() (true)
#endif

#define LOGVAR(NAME) \
    if (LOGGING_ENABLED()) then \
    { \
        diag_log format ["VAR:%1 - %2 = %3", WHEREAMI(), #NAME, NAME] \
    }
//#define LOGVAR(NAME)

//#define LOGTICK() \
//    if (LOGGING_ENABLED()) then \
//    { \
//        diag_log format ["TICK:%1 - %2", __LINE__ + 1, diag_tickTime] \
//    }

// This is a bit more helpful than the above, but it only works if LOGENTRY()
// was called so that _lastticktime can be placed in the scope, maybe a new
// directive should be added specifically for initilizating _lastticktime?
#define LOGTICK() \
    if (LOGGING_ENABLED()) then \
    { \
        diag_log format ["TICK:%1 - %2 (%3)", __LINE__ + 1, diag_tickTime, diag_tickTime - _lastticktime]; \
        _lastticktime = diag_tickTime; \
    }

#define LOGENTRY() \
    var(_lastticktime) = diag_tickTime; \
    if (LOGGING_ENABLED()) then \
    { \
        diag_log format ["ENTRY:%1", WHEREAMI()] \
    }

#define LOGMSG(msg) \
    if (LOGGING_ENABLED()) then \
    { \
        diag_log format ["MSG:%1 - %2", __LINE__ + 1, msg] \
    }

// Mutexes

#define NEW_MUTEX() \
    [false]

#define MUTEX_LOCK(mtx) \
    LOGMSG("TRYING TO LOCK"); \
    waitUntil \
    { \
        ((((mtx) select 0) isEqualTo false) \
            and { (mtx) set [0, true]; true }) \
    }; \
    LOGMSG("LOCKED")

// TODO, explode if we're trying to unlock a mutex that is not locked
#define MUTEX_UNLOCK(mtx) \
    LOGMSG("UNLOCKING"); \
    mtx set [0, false]

#define MUTEX_IS_LOCKED(mtx) \
    (((mtx) select 0) isEqualTo true)

/* Usage for above (todo, place this somewhere else)
[] spawn
{

    g_mutex_array = [NEW_MUTEX(), NEW_MUTEX(), NEW_MUTEX(), NEW_MUTEX()];
    g_values = [0, 0, 0, 0];
    g_runs = [];
    for "_i" from 0 to 19 do
    {
        var(_spawn) = _i spawn
            {
                var(_idx) = (_this mod 4);
                var(_mutex) = g_mutex_array select _idx;
                MUTEX_LOCK(_mutex);
                VAR(_value) = g_values select _idx;
                sleep 0.05;
                g_values set [_idx, _value + 1];
                MUTEX_UNLOCK(_mutex);
            };
        g_runs set [_i, _spawn];
    };
    waitUntil { [{ scriptDone _this }, g_runs] call INEPT_fnc_all };
    assert (g_values isEqualTo [5, 5, 5, 5]);

};
*/

#define RANGE(start, end, inc) \
    [{_i = start}, {_i < end}, {_i = _i + inc}]
