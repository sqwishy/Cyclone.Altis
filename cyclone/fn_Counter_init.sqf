// Arguments
//   _start: Starting value (optional, default 0)
//   _max: Maximum value before starting over (optional, default 999999)

#include "common\defines.h"

params [["_start", 0], ["_max", 999999]];

[_start, _start, _max] call cy_data_Counter_init
