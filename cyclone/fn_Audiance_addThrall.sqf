#include "common\defines.h"

params ["_audiance", "_thrall"];
LOGENTRY();
MUTEX_LOCK(_audiance select cy_data_Audiance_mutex);
(_audiance select cy_data_Audiance_members) pushBack _thrall;
MUTEX_UNLOCK(_audiance select cy_data_Audiance_mutex);
