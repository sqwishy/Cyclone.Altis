#include "common\defines.h"

var(_areas) = argument_0;
var(_origin) =
    [ [_areas, {_x select inept_data_Area_pos}] call INEPT_fnc_applyToAll
    ] call cy_fnc_averagePosition;
var(_radius) = 0;
{
    var(_x_dist) = ((_x select inept_data_Area_pos) distance _origin) + (_x select inept_data_Area_width);
    if (_radius < _x_dist) then
    {
        _radius = _x_dist;
    };
}
forEach _areas;
[_radius, _origin] call inept_data_Area_init_circle
