// Elements in _source are moved to _sink when _filter_fn evaluates to true.
// Returns a list the elements that were moved.

params ["_sink", "_source", "_filter_fn"];

private _moves = [];
private _forEachIndex = count _source;
while {
    _forEachIndex = _forEachIndex - 1;
    _forEachIndex >= 0
} do {
    private _x = _source select _forEachIndex;
    if (call _filter_fn) then {
        _source deleteAt _forEachIndex;
        _sink pushBack _x;
        _moves pushBack _x;
    };
};
_moves
