#include "common\defines.h"

#define INTERVAL 10

params ["_queue"];

while { sleep INTERVAL; true } do
{
    [_queue] call cy_fnc_GC_cycle;
};
