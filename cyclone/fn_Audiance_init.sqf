#include "common\defines.h"

params ["_cyclone"];

[ NEW_MUTEX() // cy_data_Audiance_mutex     
, _cyclone	  // cy_data_Audiance_cyclone   
, []		  // cy_data_Audiance_members   
, [] 		  // cy_data_Audiance_last_value
] call cy_data_Audiance_init
