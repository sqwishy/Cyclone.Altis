cy_param_WithHeadlessClients   = ("WithHeadlessClients" call cy_fnc_getParamValueReallyThough) == 1;
cy_param_GlobalAILimit         = "GlobalAILimit" call cy_fnc_getParamValueReallyThough;
cy_param_HeadlessClientAILimit = "HeadlessClientAILimit" call cy_fnc_getParamValueReallyThough;
cy_param_PlotCountLimit        = "PlotCountLimit" call cy_fnc_getParamValueReallyThough;
cy_param_DisplayConflictMarkers = ("ConflictMarkers" call cy_fnc_getParamValueReallyThough) == 1;

cy_param_PressureWasherPlotWeight = "PressureWasherPlotWeight" call cy_fnc_getParamValueReallyThough;
cy_param_OwieKneePlotWeight       = "OwieKneePlotWeight" call cy_fnc_getParamValueReallyThough;
