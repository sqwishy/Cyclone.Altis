params ["_thrall", "_plot", "_fn_idx", "_ex_args", "_default"];

private _mapping =
    (_thrall select cy_data_Thrall_cyclone
             select cy_data_Cyclone_drivers
             select (_plot select cy_data_Plot_driver_serial)
             select cy_data_PlotDriver_mapping);
if (count _mapping > _fn_idx) then
{
    private _fn = _mapping select _fn_idx;
    if !(isNil "_fn") then
    {
        private _args = [_thrall, _plot];
        _args append _ex_args;
        (_args call _fn)
    }
    else
    {
        _default
    }
}
else
{
    _default
}
