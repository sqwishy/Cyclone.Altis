#include "common\defines.h"

params ["_audiance_var", "_plot"];

LOGENTRY();

// This is probably needed for JIP players. The remoteExec that invokes this is
// persistent, but it may be called before the audiance has been set up.
waitUntil {sleep 0.1; not isNil _audiance_var};

private _audiance = missionNamespace getVariable _audiance_var;
private _plot_data_var = [_audiance, _plot] call cy_fnc_Audiance_plotDataVar;

MUTEX_LOCK(_audiance select cy_data_Audiance_mutex);

if !(isNil _plot_data_var) then
{
	LOGVAR(_audiance select cy_data_Audiance_last_value);
	[ _audiance select cy_data_Audiance_last_value
	, {not (_x isEqualTo _plot_data_var)}
	] call inept_fnc_conditionalSelectIP;
	LOGVAR(_audiance select cy_data_Audiance_last_value);
};

[missionNamespace getVariable _plot_data_var, _plot] call cy_fnc_copyInto;

MUTEX_UNLOCK(_audiance select cy_data_Audiance_mutex);
