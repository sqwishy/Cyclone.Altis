/* cy_fnc_DepotItem_init
 *
 * Parameters:
 *  0: An array of CfgVehicle things or a CfgGroup entry.
 *  1: An array of unit infos corresponding to the first argument.
 *  2: Cost multiplier; 1 does nothing, 0 makes it free, 2 doubles
 *     the cost. Higher cost items tend to be avoided a bit by
 *     cy_fnc_Depot_spawnForces. Optional. Defaults to 1.
 */

#include "common\defines.h"

params ["_source"
       ,"_unit_infos"
       ,["_cost_mod", 1]];

// This is a farily arbitrary thing I threw in here to provide some further
// weighting to discourage the system from just trying to solve all its
// problems with heavy armor.
var(_cost) = 0;
{
    var(_clsname) = _x select inept_data_UnitInfo_class;
    _cost = _cost +
        (switch (true) do
        {
            case (_clsname isKindOf ["Air", configFile >> "CfgVehicles"]):
            {
                9
            };
            case (_clsname isKindOf ["Tank", configFile >> "CfgVehicles"]):
            {
                7
            };
            case (_clsname isKindOf ["Car", configFile >> "CfgVehicles"]):
            {
                3
            };
            default
            {
                1
            };
        });
}
forEach _unit_infos;

// cy_data_DepotItem_source
[ _source
// cy_data_DepotItem_infos
, _unit_infos
// cy_data_DepotItem_firepower
, [_unit_infos] call INEPT_fnc_getCombinedFirepower
// cy_data_DepotItem_cost
, _cost * _cost_mod
] call cy_data_DepotItem_init
