#include "common\defines.h"

params ["_cyclone", "_key", "_plot"];

LOGMSG(_fnc_scriptNameParent);
LOGMSG(_fnc_scriptName);
[ _cyclone select cy_data_Cyclone_magic_prefix
, _key
, (_plot select cy_data_Plot_id) call cy_fnc_toBase36
] joinString "."
