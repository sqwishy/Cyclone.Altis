// Arguments
//  _narrator:
//      Narrator (cy_data_Narrator)
//  _driver:
//      Driver (cy_data_PlotDriver)
//  _hc_slot: 
//      Headless Client Slot Object Thingy

#include "common\defines.h"

LOGENTRY();

params ["_narrator", "_driver", "_hc_slot"];

private _id = [_narrator select cy_data_Narrator_counter] call cy_fnc_Counter_count;
private _plot = [_id, _driver select cy_data_PlotDriver_serial, _hc_slot] call cy_data_Plot_init;

private _cyclone = _narrator select cy_data_Narrator_cyclone;
private _jipid = [_cyclone, "jip", _plot] call cy_fnc_Cyclone_plotVar;
LOGMSG(_fnc_scriptName);
LOGVAR(_jipid);
LOGMSG("cy_fnc_Audiance_updatePlotByRemote");
LOGVAR(_plot);
[_narrator select cy_data_Narrator_audiance_var, _plot]
    remoteExec ["cy_fnc_Audiance_updatePlotByRemote", REMOTE_EXEC_EVERYWHERE, _jipid];

private _plot_gc_var = [_cyclone, "gc", _plot] call cy_fnc_Cyclone_plotVar;
missionNamespace setVariable [_plot_gc_var, []];

_plot
