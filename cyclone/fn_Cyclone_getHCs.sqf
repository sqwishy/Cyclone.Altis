// Returns non-nil non-null vaules found matching the HC slot names that the
// Cyclone was initilized with.

#include "common\defines.h"

params ["_cyclone"];

[ [ _cyclone select cy_data_Cyclone_hc_slots
  , { missionNamespace getVariable [_x, objNull] }
  ] call INEPT_fnc_applyToAll
, { not isNull _x }
] call INEPT_fnc_conditionalSelectIP
