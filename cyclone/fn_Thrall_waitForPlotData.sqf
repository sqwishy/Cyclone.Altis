params ["_thrall", "_plot", "_key"];

private _var = [_thrall, _plot select cy_data_Plot_id] call cy_fnc_Thrall_plotletVar;
if !(isNil "_key") then
{
    _var = [_var, _key] joinString "_";
};

private "_data";
waitUntil
{
    _data = missionNamespace getVariable _var;
    (not isNil "_data") or
    {sleep 0.1; diag_log (format ["%1 waiting for %2", _fnc_scriptNameParent, _var]); false}
};

//#define SIMULATE_MP
#ifdef SIMULATE_MP
[] +
#endif
_data
