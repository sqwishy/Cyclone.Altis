#include "common\defines.h"

// At some point this might be extended so that things can be retrieved out of
// the GC based on their key.

params ["_thrall", "_plot", "_thing", ["_key", ""]];

[_thrall select cy_data_Thrall_narrator_var, _plot, _thing]
	remoteExec ["cy_fnc_Narrator_addToPlotGCByRemote", SERVER_CID];
