#include "common\defines.h"

#define MIN_PLAYER_DIST 1200

params ["_queue"];

private _i = count _queue;
while {_i = _i - 1; _i >= 0} do
{
    private _v = _queue select _i;
    switch (true) do
    {
    case (isNull _v):
    {
        _queue deleteAt _i;
    };
    case (_v isEqualType grpNull):
	{
		private _units = units _v;
		if !(_units isEqualTo []) then
		{
			[_units] call cy_fnc_GC_cycle;
		};
		if (_units isEqualTo []) then
		{
			deleteGroup _v;
			_queue deleteAt _i;
		};
	};
    case ((typeName _v) isEqualTo "LOCATION"):
    {
        deleteLocation (_v);
        _queue deleteAt _i;
    };
    case (_v isEqualType objNull):
	{
		if ([allPlayers, {(_x distance _v) > MIN_PLAYER_DIST}] call INEPT_fnc_all) then
		{
			deleteVehicle _v;
			// Sometimes things leave a corpse or something, make sure it's
			// super dead.
			if (alive _v) then
			{
				_v setDamage 1;
			};
			_queue deleteAt _i;
		};
	};
	default
	{
		["No idea how to GC %1 - removing it from the queue.", _v] call bis_fnc_error;
        _queue deleteAt _i;
	}
    };
};
