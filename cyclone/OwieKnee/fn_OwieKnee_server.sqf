#define DEBUG_VAR "cy_fnc_OwieKnee_server_is_verbose"

#include "..\common\defines.h"

params ["_thrall", "_plot", "_map", "_side", "_safe_zones"];

private _num_patients = 8 + floor random 6;
private _patients_status = [];

private _task_pos = [];
while {_task_pos isEqualTo []} do
{
    private _spot = [_map] call cy_fnc_Map_randomSpot;
    if (getTerrainHeightASL _spot > 0) then
    {
        private _bld_spot = position (nearestBuilding _spot);
        // Find the nearest building, hopefully it's the edge of an urban area,
        // and elect the spot about 100 metres "inward".
        _spot set [2, 0];
        _spot = _bld_spot vectorAdd ((_spot vectorFromTo _bld_spot) vectorMultiply (30 + random 170));
        private _urbanyness = [_spot, 60, 10] call INEPT_fnc_buildingDensity;
        if (_urbanyness > 0.6) then
        {
            // "Land_VR_Block_03_F" being rougly the size of a crowd of people ...
            // Of course this is completely bogys, findEmptyPosition doesn't
            // seem to work reliably all in practice... not sure what an
            // effective alternative is.
            _spot = _spot findEmptyPosition [0, 40, "Land_VR_Block_03_F"];
            _task_pos = _spot;
        };
    };
};

private _plotlet =
    [_side, _task_pos] call cy_data_OwieKnee_init;

LOGVAR(_plotlet);

[_thrall, _plot, _plotlet] call cy_fnc_Thrall_sendPlotData;

private _i = -1;
while {inc(_i); _i < _num_patients} do
{
    _patients_status pushBack cy_const_OwieKnee_untreated;
};
[_thrall, _plot, _patients_status, "status"] call cy_fnc_Thrall_sendPlotData;

private _patients = [_thrall, _plot, "patients"] call cy_fnc_Thrall_waitForPlotData;

private _done = false;
while
{
    sleep 1;
    (not _done)
    and {not ([_plot] call cy_fnc_Plot_isCancelled)}
}
do
{
    private _last_status = [] + _patients_status;

    // I'm not sure if we want to keep checking if people have died after
    // they're treated ...
    {
        _patients_status set [
            _forEachIndex,
            switch (true) do
            {
            case (not alive _x):
                {cy_const_OwieKnee_dead};
            case (not ([_x] call cy_fnc_OwieKnee_isUnitTreated)):
                {cy_const_OwieKnee_untreated};
            default
                {cy_const_OwieKnee_treated};
            }
        ];
    }
    forEach _patients;

    if !(_last_status isEqualTo _patients_status) then
    {
        [_thrall, _plot, _patients_status, "status"] call cy_fnc_Thrall_sendPlotData;
    };

    private _num_untreated = {_x == cy_const_OwieKnee_untreated} count _patients_status;
    _done = _num_untreated == 0;
};
