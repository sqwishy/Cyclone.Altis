params ["_unit"];
(alive _x)
and {
    if (cy_var_is_ace_enabled) then
    {
        [ _unit getVariable ["ace_medical_bodyPartStatus", []]
        , {_x < 0.05}
        ] call inept_fnc_all
    }
    else
    {
        damage _x < 0.10
    }
}
