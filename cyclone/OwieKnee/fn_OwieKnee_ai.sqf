#include "..\common\defines.h"

#define INJURED_ANIMS \
    ["Acts_InjuredAngryRifle01", "Acts_InjuredLookingRifle04", "Acts_InjuredLookingRifle05", "Acts_LyingWounded_loop2", "Acts_LyingWounded_loop3"]

#define ACE_DAMAGE_CAUSES \
    ["bite","bullet","explosive","falling","grenade","punch","stab","falling"]

// Head and body are ignored because weird things happen and people die at
// surprisingly low damage values.
#define ACE_HIT_PARTS \
    ["hand_l", "hand_r", "leg_l", "leg_r"]

params ["_thrall", "_plot", "_depot", "_baddie_ai_profile", "_civ_ai_profile"];

private _plotlet         = [_thrall, _plot] call cy_fnc_Thrall_waitForPlotData;
private _side            = _plotlet select cy_data_OwieKnee_side;
private _task_pos        = _plotlet select cy_data_OwieKnee_task_pos;
private _patients_status = [_thrall, _plot, "status"] call cy_fnc_Thrall_waitForPlotData;

// We don't really know how long these could take, so we create the heartbeat
// afterward.

private _untreated_group = grpNull;
private _treated_group = grpNull;

private _cleanup_units = [];
private _hb = [cy_var_plot_suggested_hb_lifetime] call cy_fnc_Heartbeat_init;

[_hb, _cleanup_units] spawn
{
    params ["_hb", "_cleanup_units"];
    waitUntil {sleep 1; ([_hb] call cy_fnc_Heartbeat_isDead)};
    _cleanup_units call cy_fnc_GC;
};

private _patient_classes =
    [ _patients_status
    , { cy_var_OwieKnee_civilian_types call BIS_fnc_selectRandom }
    ] call INEPT_fnc_applyToAll;

private _town_area =
    [ 50
    , _task_pos
    ] call inept_data_Area_init_circle;

_untreated_group =
    [_depot, _town_area, civilian, _patient_classes, _civ_ai_profile] call cy_fnc_Depot_spawnUnits;
[_thrall, _plot, _untreated_group] call cy_fnc_Thrall_addToPlotGC;

{
    [_hb] call cy_fnc_Heartbeat_beat;

    private _unit = _x;

    if (cy_var_is_ace_enabled) then
    {
        private _parts = ACE_HIT_PARTS;
        [ _unit, 0.30 + random 0.4
        , _parts call inept_fnc_popRandom
        , ACE_DAMAGE_CAUSES call inept_fnc_popRandom
        ] call ACE_Medical_fnc_addDamageToUnit;
        if (random 1 < 0.5) then
        {
            [ _unit, 0.70
            , _parts call inept_fnc_popRandom
            , ACE_DAMAGE_CAUSES call inept_fnc_popRandom
            ] call ACE_Medical_fnc_addDamageToUnit;
        };
        if (random 1 < 0.5) then
        {
            [ _unit, 0.30
            , _parts call inept_fnc_popRandom
            , ACE_DAMAGE_CAUSES call inept_fnc_popRandom
            ] call ACE_Medical_fnc_addDamageToUnit;
        };
    }
    else
    {
        _unit setDamage (0.5 + random 0.3);
    };

    _unit setSpeaker "NoVoice";

    // Give them "Offroad_01_base_F" amount of space so they aren't too clutterd.
    private _patient_pos = _task_pos findEmptyPosition [0, 30, "Offroad_01_base_F"];
    if (_patient_pos isEqualTo []) then
    {
        _patient_pos = _task_pos;
    };

    _unit disableAI "AUTOTARGET";
    _unit disableAI "TARGET";
    _unit disableAI "MOVE";
    _unit disableAI "ANIM";
    _unit setPosATL ([_patient_pos, random 1.5] call INEPT_fnc_deviatedVector);
    _unit setDir (random 360);
    _unit setVariable [cy_var_OwieKnee_animation, INJURED_ANIMS call BIS_fnc_selectRandom, true];
    if (not hasInterface) then
    {
        // We need to do the switchMove even if we are a dedicated server
        // or headless client. Otherwise the guys will just get back up
        // again it seems.
        _unit switchMove (_unit getVariable cy_var_OwieKnee_animation);
    };

    // sleep because findEmptyPosition won't account for the other guys we
    // moved if there isn't some sort of pause here I guess ...
    sleep 0.15;
}
forEach (units _untreated_group);

assert (count units _untreated_group == count _patients_status);

[_thrall, _plot, units _untreated_group, "patients"] call cy_fnc_Thrall_sendPlotData;

_baddie_groups =
    [ _depot
    , [600, _task_pos] call inept_data_Area_init_circle
    , _side
    // Forces should be good fighting against the following pairs of
    // composition and lethality for infantry, armour, aircraft
    , [ 16 + random 6, 20 + random 4,
        4,             6 + random 4,
        6,             2
      ] call inept_data_Firepower_init
    , _baddie_ai_profile
    ] call cy_fnc_Depot_spawnForces;
[_thrall, _plot, _baddie_groups] call cy_fnc_Thrall_addToPlotGC;

private _ao = [_side] call INEPT_fnc_newAO;

private _main_aoi =
    [ [80, _task_pos] call inept_data_Area_init_circle
    , [8 + random 6, 2, 0]
    , false // Is a reserve AoI?
    , 1     // BEHAVIOR_DEFEND (sorry for using the literal here)
    ] call INEPT_fnc_newAoI;

private _patrol_aoi =
    [ [800, _task_pos] call inept_data_Area_init_circle
    , [4, 2, 2]
    , true  // Is a reserve AoI?
    , 1     // BEHAVIOR_DEFEND (sorry for using the literal here)
    ] call INEPT_fnc_newAoI;

[_ao, _patrol_aoi] call INEPT_fnc_addAoI;
[_ao, _main_aoi] call INEPT_fnc_addAoI;

LOGVAR(_baddie_groups);
{
    [_ao, _x] call INEPT_fnc_addGroup;
}
forEach _baddie_groups;

_cleanup_units append (units _untreated_group);
{
    _cleanup_units append (units _x);
}
forEach _baddie_groups;

[_hb] call cy_fnc_Heartbeat_beat;

[_ao] spawn
{
    params ["_ao"];
    while {
        sleep cy_var_suggested_tick_interval;
        ([_ao] call INEPT_fnc_hasPawns)
    } do {
        // Use a spawn to avoid stopping the loop in the event of catastrophic
        // failure ... I guess ...
        private _handle = [_ao] spawn INEPT_fnc_processAOOnce;
        waitUntil {sleep 0.05; scriptDone _handle};
    };
};

private __fn_get_treated_group =
{
    // Delay the creation of the treated group so that it doesn't get
    // automatically cleaned up when empty.
    if (isNull _treated_group) then
    {
        _treated_group = createGroup civilian;
        [_thrall, _plot, _treated_group] call cy_fnc_Thrall_addToPlotGC;
        while {!((waypoints _treated_group) isEqualTo [])} do
        {
            deleteWaypoint ((waypoints _treated_group) select 0);
        };
        private _wp = _treated_group addWaypoint [_task_pos, 0];
        _wp setWaypointType "DISMISS";
        _treated_group setCurrentWaypoint _wp;

        _treated_group setCombatMode "GREEN";
        _treated_group setFormation "DIAMOND";
        _treated_group allowFleeing 0;
    };
    _treated_group
};

private _untreated = units _untreated_group;

private _done = false;
while
{
    sleep 1;
    (not _done)
    and {[_hb] call cy_fnc_Heartbeat_beat}
    and {not ([_plot] call cy_fnc_Plot_isCancelled)}
}
do
{
    private _became_treated =
        [[], _untreated, {[_x] call cy_fnc_OwieKnee_isUnitTreated}] call cy_fnc_sieveIP;
    {
        _x enableAI "MOVE";
        _x enableAI "ANIM";
        [_x] joinSilent (call __fn_get_treated_group);
        if (not hasInterface) then
        {
            _x switchMove "AmovPpneMstpSnonWnonDnon";
        };
    }
    forEach _became_treated;

    _patients_status = [_thrall, _plot, "status"] call cy_fnc_Thrall_getPlotData;
    private _num_untreated = {_x == cy_const_OwieKnee_untreated} count _patients_status;
    _done = _num_untreated == 0;
};
