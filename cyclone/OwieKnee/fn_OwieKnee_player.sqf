// #define DEBUG_VAR "cy_fnc_OwieKnee_player_is_verbose"

#include "..\common\defines.h"

#define TASK_DESC \
 "The local civilian population at %1 is in need of medical assistance. Reports\
 indicate that many have sustained injuries from several sources, including\
 military conflicts in nearby regions, %2, and %3.\
 We have decided to use this opportunity to win their affection by coming to\
 their aid."

#define PROGRESS_FULL_TASK_DESC \
 "Treat all (%1) injured civilians."

#define PROGRESS_MOST_TASK_DESC \
 "Treat three quarters (%1) of the injured civilians."

#define PROGRESS_HALF_TASK_DESC \
 "Treat half (%1) of the injured civilians."

private _INJURY_REASONS =
    [ "trying to wash the cat"
    , "trying to put on their makeup while driving"
    , "practicing their tapdance while in the shower"
    , "fighting over the television remote"
    , "general slapstick"
    , "throwing a CTR monitor out a window in a fit of rage and pulling a muscle"
    , "having a CRT monitor land on their head"
    , "making a face and it getting stuck like that"
    , "getting beaten up by a girl"
    ];

params ["_thrall", "_plot"];

// Wait for all the data before displaying tasks.

private _plotlet  = [_thrall, _plot] call cy_fnc_Thrall_waitForPlotData;
private _side     = _plotlet select cy_data_OwieKnee_side;
private _task_pos = _plotlet select cy_data_OwieKnee_task_pos;
private _patients_status = [_thrall, _plot, "status"] call cy_fnc_Thrall_waitForPlotData;
private _patients = [_thrall, _plot, "patients"] call cy_fnc_Thrall_waitForPlotData;

LOGVAR(_patients);

private _task_root = [_plot select cy_data_Plot_id, "tsk"] joinString "_";

private _task_desc = format
    [ TASK_DESC
    , _task_pos call BIS_fnc_locationDescription
    , _INJURY_REASONS call INEPT_fnc_popRandom
    , _INJURY_REASONS call INEPT_fnc_popRandom ];

[ _task_root
, player
, [_task_desc, "Aid Civilians", ""]
, _task_pos
, "AUTOASSIGNED"
, nil       // Priority
, true      // Show notifications if true
, false     // Global if true else local
, "Defend"  // CfgTaskTypes
, true      // disabled shared objective counter if false else enabled it
] call BIS_fnc_setTask;

private _num_treated = {_x == cy_const_OwieKnee_treated} count _patients_status;
private _num_untreated = {_x == cy_const_OwieKnee_untreated} count _patients_status;

private _task_half = [_task_root, "half"] joinString "_";
private _task_most = [_task_root, "most"] joinString "_";
private _task_all  = [_task_root, "all"] joinString "_";

{
    _x params ["_task", "_task_text"];
    [ [_task, _task_root]
    , player
    , ["", _task_text, ""]
    , []
    , "AUTOASSIGNED"
    , nil       // Priority
    , false     // Show notifications if true
    , false     // Global if true else local
    , "Defend"  // CfgTaskTypes
    , true      // disabled shared objective counter if false else enabled it
    ] call BIS_fnc_setTask;
}
forEach [ [_task_half, "Treat half of the civilians"]
        , [_task_most, "Treat most of the civilians"]
        , [_task_all,  "Treat all of the civilians"] ];

private _total_patients = count _patients_status;

private ["_anim"];
{
    waitUntil
    {
        (not alive _x)
        or {_anim = _x getVariable cy_var_OwieKnee_animation; (not isNil "_anim")}
        or {sleep 0.05; false}
    };
    if (alive _x) then
    {
        _x setSpeaker "NoVoice";
        _x switchMove _anim;
    };
}
forEach _patients;

private _treated = [];
private _untreated = _patients + [];

private _is_first_loop = true;
private _done = false;
while
{
    if !(_is_first_loop) then {sleep 1};
    (not _done)
    and {not ([_plot] call cy_fnc_Plot_isCancelled)}
}
do
{
    private _became_treated =
        [_treated, _untreated, {[_x] call cy_fnc_OwieKnee_isUnitTreated}] call cy_fnc_sieveIP;
    if !(_is_first_loop) then
    {
        {
            _x switchMove "AmovPpneMstpSnonWnonDnon";
        }
        forEach _became_treated;
    };

    _patients_status = [_thrall, _plot, "status"] call cy_fnc_Thrall_getPlotData;

    _num_treated = {_x == cy_const_OwieKnee_treated} count _patients_status;
    _num_untreated = {_x == cy_const_OwieKnee_untreated} count _patients_status;
    _num_dead = {_x == cy_const_OwieKnee_dead} count _patients_status;

    {
        _x params ["_task", "_threshold"];
        private _status =
            switch (true) do
            {
            case (_total_patients - _num_dead < _threshold):
                {"FAILED"};
            case (_num_treated >= _threshold):
                {"SUCCEEDED"};
            default
                {"CREATED"};
            };
        [ [_task, _task_root]
        , player
        , nil
        , nil
        , _status
        , nil       // Priority
        , not _is_first_loop // Show notifications if true
        , false     // Global if true else local
        , nil       // CfgTaskTypes
        , true      // disabled shared objective counter if false else enabled it
        ] call BIS_fnc_setTask;
    } forEach [ [_task_half, 0.50 * _total_patients]
              , [_task_most, 0.75 * _total_patients]
              , [_task_all, _total_patients] ];

    _is_first_loop = false;
    _done = _num_untreated == 0;
};

switch (true) do
{
case (_done):
{
    LOGMSG("plot is done");
    [ _task_root
    , player
    , nil
    , nil
    , if (_num_treated > 0 /* Really low standards :) */) then {"SUCCEEDED"} else {"FAILED"}
    , nil       // Priority
    , true      // Show notifications if true
    , false     // Global if true else local
    , nil       // CfgTaskTypes
    , true      // disabled shared objective counter if false else enabled it
    ] call BIS_fnc_setTask;
};
case ([_plot] call cy_fnc_Plot_isCancelled):
{
    LOGMSG("plot is cancelled");
    ["TaskCanceled", ["", "..."]] call BIS_fnc_showNotification;
    {
        private _task = player getVariable (_x call BIS_fnc_taskVar);
        if !((taskState _task) in ["SUCCEEDED", "FAILED"]) then
        {
            _task setTaskState "CANCELED";
        };
    }
    forEach [_task_root, _task_half, _task_most, _task_all];
};
};

[_task_root, _task_half, _task_most, _task_all] spawn
{
    sleep 30;
    {
        // BIS_fnc_deleteTask does not work here and all of BIS's task bullshit
        // is too overly complex to debug.
        player removeSimpleTask (player getVariable (_x call BIS_fnc_taskVar));
    }
    forEach _this;
};
