#include "..\platform.sqf"

cy_plot_OwieKnee =
    [ "cy_fnc_OwieKnee_server"
    , "cy_fnc_OwieKnee_player"
    , "cy_fnc_OwieKnee_ai" ];

cy_plot_OwieKnee set [cy_plot_fnc_isPosInUse, cy_fnc_OwieKnee_isPosInUse];
cy_plot_OwieKnee set [
    cy_plot_fnc_isAllowedQuickEntryAtPos,
    {not (_this call cy_fnc_OwieKnee_isPosInUse)}
    ];

cy_data_OwieKnee                   = DATA();
cy_data_OwieKnee_init              = pl_data_init;
cy_data_OwieKnee_side              = ATTR(cy_data_OwieKnee);
cy_data_OwieKnee_task_pos          = ATTR(cy_data_OwieKnee);

cy_var_OwieKnee_animation = "cy_OW_anim";
cy_var_OwieKnee_area_use_radius = 300;
// Should be modified for different maps/settings
cy_var_OwieKnee_civilian_types =
    ["C_man_1", "C_man_1_1_F", "C_man_p_beggar_F", "C_man_polo_1_F",
    "C_man_polo_2_F", "C_man_polo_3_F", "C_man_polo_4_F", "C_man_polo_5_F",
    "C_man_polo_6_F"];

cy_const_OwieKnee_untreated = 0;
cy_const_OwieKnee_treated = 1;
cy_const_OwieKnee_dead = 2;
