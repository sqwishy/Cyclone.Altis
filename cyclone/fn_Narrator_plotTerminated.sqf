#include "common\defines.h"

params ["_narrator", "_plot"];

LOGVAR(_plot);
LOGMSG("Plot has terminated on Narrator, removing from registry.");

private _cyclone = _narrator select cy_data_Narrator_cyclone;
private _jipid = [_cyclone, "jip", _plot] call cy_fnc_Cyclone_plotVar;
LOGMSG(_fnc_scriptName);
LOGVAR(_jipid);
LOGMSG("cy_fnc_Audiance_removePlotByRemote");
LOGVAR(_plot);
[_narrator select cy_data_Narrator_audiance_var, _plot]
	remoteExec ["cy_fnc_Audiance_removePlotByRemote", REMOTE_EXEC_EVERYWHERE, _jipid];

// Remove from JIP remoteExec queue thingy
remoteExec ["" , _jipid];

private _plot_gc_var = [_cyclone, "gc", _plot] call cy_fnc_Cyclone_plotVar;
// Add the plot's leftovers to the narrator's main gc queue so it will be
// collected.
(_narrator select cy_data_Narrator_gc_queue) append (missionNamespace getVariable _plot_gc_var);
missionNamespace setVariable [_plot_gc_var, nil];
