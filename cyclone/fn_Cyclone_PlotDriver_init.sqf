#include "common\defines.h"

params ["_cyclone", "_mapping", ["_weight", 1]];

private _driver = [];
private _serial = (_cyclone select cy_data_Cyclone_drivers) pushBack _driver;
_driver append
    // cy_data_PlotDriver_mapping
    [ _mapping
    // cy_data_PlotDriver_serial
    , _serial
    // cy_data_PlotDriver_weight
    , _weight
    // cy_data_PlotDriver_server_args
    , []
    // cy_data_PlotDriver_player_args
    , []
    // cy_data_PlotDriver_ai_args
    , []
    ];
_driver
