=============
Cyclone.Altis
=============

Cyclone is some sort of persistent cooperative scenario for Arma 3. Similar to
Invade & Annex, but not nearly as mature.

This is the source for Cyclone and an its application on the Altis map. Cyclone
is designed to be fairly map independent, with most of its features properly
isolated in the cyclone directory. Everything else is just configuration (in
theory).

Cyclone was created to demonstrate an AI director called ineptifex. As such,
that is a dependency -- although loosely -- and a copy of it is included in
this repository. Aside from an up-to-date version of Arma 3, that is the only
dependency. Therefore, the format of this project is such that you should be
able to open it up in the editor and run it.

This is licensed under the GPLv3 -- because sharing is caring. A copy of the
GPL has been provided in the LICENCE file in this directory.


Mission Parameters
------------------

When :code:`call cy_fnc_paramsInit` in executed in a scheduled environment it
will block until the parameter variables are assigned. This is required for the
Cyclone runtime to work correctly.

Global AI Limit:
    Missions are no longer created once the number of AI on the map exceed this
    limit.

    Key:
        GlobalAILimit
    Default:
        40
    Assigned To:
        :code:`cy_param_GlobalAILimit`

Headless Clients:
    When enabled, AI will only be local to headless clients and not the server.
    When disabled, they will only be local to the server. This is implemented
    by running the `_ai` plot scripts only on selective machines.

    Key:
        WithHeadlessClients
    Default:
        Disabled (:code:`false`)
    Assigned To:
        :code:`cy_param_WithHeadlessClients` *as a boolean*

Headless Client AI Limit:
    Missions are only created and assigned connected headless clients if they
    have fewer AI local to them than this number. In single player or when
    :code:`cy_param_WithHeadlessClients` is false, this parameter is ignored.

    Key:
        HeadlessClientAILimit
    Default:
        40
    Assigned To:
        :code:`cy_param_HeadlessClientAILimit`

Maximum Concurrent Plots:
    Key:
        PlotCountLimit
    Default:
        3
    Assigned To:
        :code:`cy_param_PlotCountLimit`

Display Conflict Markers:
    Key:
        ConflictMarkers
    Default:
        Invisible (:code:`false`)
    Assigned To:
        :code:`cy_param_DisplayConflictMarkers`

A plot with a weight of 2 is twice as likly to occur compared to a weight of 1.

Pressure Washer Plot Weight:
    Key:
        PressureWasherPlotWeight
    Default:
        2
    Assigned To:
        :code:`cy_param_PressureWasherPlotWeight`

Medical Mission Plot Weight:
    Key:
        OwieKneePlotWeight
    Default:
        1
    Assigned To:
        :code:`cy_param_OwieKneePlotWeight`
