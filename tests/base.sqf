private __assertIsEqual =
{
    params ["_line", "_test", "_expected"];
	private _result = call _test;
    if (_result isEqualTo _expected) then
    {
        diag_log format ["PASS:%1 assertIsEqual", _line + 1];
    }
    else
    {
        diag_log format ["FAIL:%1 assertIsEqual - %2 => %3", _line + 1, _test, _result];
        diag_log format ["FAIL:%1 assertIsEqual - Expected: %2", _line + 1, _expected];
        throw "Failure";
    };
};

private __assertIsNotNull =
{
    params ["_line", "_check"];
    private _value = call _check;
    if !(isNull _value) then
    {
        diag_log format ["PASS:%1 assertIsNotNull - %2", _line + 1, _check];
    }
    else
    {
        diag_log format ["FAIL:%1 assertIsNotNull - %2", _line + 1, _check];
        diag_log format ["FAIL:%1 assertIsNotNull - %2", _line + 1, _value];
        throw "Failure";
    };
};

private __assertIsNull =
{
    params ["_line", "_check"];
    private _value = call _check;
    if (isNull _value) then
    {
        diag_log format ["PASS:%1 assertIsNull - %2", _line + 1, _check];
    }
    else
    {
        diag_log format ["FAIL:%1 assertIsNull - %2", _line + 1, _check];
        diag_log format ["FAIL:%1 assertIsNull - %2", _line + 1, _value];
        throw "Failure";
    };
};
