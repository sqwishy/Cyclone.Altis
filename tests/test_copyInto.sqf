#include "..\cyclone\common\defines.h"

#include "base.sqf"

private _test_copyInto_simple =
{
	private _dst = ["foo", 321];
	[_dst, ["foo", 123]] call cy_fnc_copyInto;
    [__LINE__, _dst, ["foo", 123]] call __assertIsEqual;
};

private _test_copyInto_recursive =
{
	private _dst = [0, [1, 2], 3];
	private _nested = _dst select 1;
	[_dst, [4, [5, 6], 7]] call cy_fnc_copyInto;
    [__LINE__, _dst, [4, [5, 6], 7]] call __assertIsEqual;
    [__LINE__, _nested, [5, 6]] call __assertIsEqual;
};

["cy_fnc_copyInto"] call bis_fnc_recompile;

try
{
    call _test_copyInto_simple;
    call _test_copyInto_recursive;
    LOGMSG("All passed?");
}
catch
{
    LOGMSG(_exception);
};
