// This file is really only supposed to contain information specific to this
// particular install of Cyclone. Stuff like the HC support code and building
// classes for the pressure washer plot are generic and should not be in here.
// This file is to abstract Cyclone from its context, like the map.

#include "cyclone\common\defines.h"

zoos setVariable ["showNotification", false];

safe_zones =
    [ [[SAFE_ZONE] call INEPT_fnc_triggerToArea] call INEPT_fnc_locationFromArea
    , [[SAFE_ZONE_1] call INEPT_fnc_triggerToArea] call INEPT_fnc_locationFromArea
    , [[SAFE_ZONE_2] call INEPT_fnc_triggerToArea] call INEPT_fnc_locationFromArea
    ];

ai_profile_opfor =
    [ ["aimingAccuracy", 0.25, 0.2]
    , ["aimingShake",    0.25, 0.1]
    , ["aimingSpeed",    0.15, 0.2]
    , ["endurance",      0.20, 0.7]
    , ["spotDistance",   0.40, 0.2]
    , ["spotTime",       0.30, 0.2]
    , ["courage",        0.40, 0.5]
    , ["reloadSpeed",    0.10, 0.4]
    , ["commanding",     0.30, 0.4]
    , ["general",        0.50, 0.5] ];

ai_profile_civilian =
    [ ["aimingAccuracy", 0.05, 0.1]
    , ["aimingShake",    0.05, 0.1]
    , ["aimingSpeed",    0.05, 0.2]
    , ["endurance",      0.40, 0.4]
    , ["spotDistance",   0.10, 0.2]
    , ["spotTime",       0.20, 0.2]
    , ["courage",        0.01, 0.1]
    , ["reloadSpeed",    0.05, 0.1]
    , ["commanding",     0.00, 0.0]
    , ["general",        0.00, 0.0] ];

//// Cyclone Initilization and setup.

0 spawn
{
    waitUntil {isServer || !isNull player};
    call cy_fnc_paramsInit;

    private _hc_slots = ["HC_SLOT_0", "HC_SLOT_1", "HC_SLOT_2", "HC_SLOT_3", "HC_SLOT_4", "HC_SLOT_5"];

    cy_cyclone = [SERVER_LOGIC, _hc_slots] call cy_fnc_Cyclone_init;

    cy_audiance = [cy_cyclone] call cy_fnc_Audiance_init;

    cy_plot_PressureWasher_driver =
        [cy_cyclone, cy_plot_PressureWasher, cy_param_PressureWasherPlotWeight] call cy_fnc_Cyclone_PlotDriver_init;

    cy_plot_OwieKnee_driver =
        [cy_cyclone, cy_plot_OwieKnee, cy_param_OwieKneePlotWeight] call cy_fnc_Cyclone_PlotDriver_init;

    // More than one of these blocks can be true on a single client, so be
    // careful about clobbering variables.

    if (cy_cyclone select cy_data_Cyclone_is_narrator) then
    {
        var(_map) = [[30720, 30720]] call cy_data_Map_init;

        cy_plot_PressureWasher_driver set [cy_data_PlotDriver_server_args, [_map, east, safe_zones]];

        cy_plot_OwieKnee_driver set [cy_data_PlotDriver_server_args, [_map, east, safe_zones]];

        cy_narrator =
            [ cy_cyclone
            , "cy_audiance"
            , _hc_slots
            ] call cy_fnc_Narrator_init;

        var(_server_thrall) =
            [ cy_cyclone
            , cy_var_PlotRole_server
            , "cy_narrator"
            ] call cy_fnc_Thrall_init;

        [cy_audiance, _server_thrall] call cy_fnc_Audiance_addThrall;

        0 spawn 
        {
            [cy_narrator] call cy_fnc_Narrator_runForever;
            ["Cyclone stopped!"] call BIS_fnc_error;
        };
    };

    if (cy_cyclone select cy_data_Cyclone_is_player) then
    {
        var(_player_thrall) =
            [ cy_cyclone
            , cy_var_PlotRole_player
            , "cy_narrator"
            ] call cy_fnc_Thrall_init;

        [cy_audiance, _player_thrall] call cy_fnc_Audiance_addThrall;

        // Make global for _halo to use and stuff.
        cy_player_thrall = _player_thrall;
    };

    if (cy_cyclone select cy_data_Cyclone_is_AI) then
    {
        var(_depot) =
            [ cy_cyclone
            , "cy_narrator"
            , call compile preprocessFileLineNumbers "depot_opfor_pool.sqf"
            ] call cy_fnc_Depot_init;

        cy_plot_PressureWasher_driver set [cy_data_PlotDriver_ai_args, [_depot, ai_profile_opfor]];

        cy_plot_OwieKnee_driver set [cy_data_PlotDriver_ai_args, [_depot, ai_profile_opfor, ai_profile_civilian]];

        var(_ai_thrall) =
            [ cy_cyclone
            , cy_var_PlotRole_ai
            , "cy_narrator"
            ] call cy_fnc_Thrall_init;

        [cy_audiance, _ai_thrall] call cy_fnc_Audiance_addThrall;
    };
};

//// Non Cyclone stuff

is_ace_enabled = isClass (configfile >> "CfgPatches" >> "ace_common");

// ACE settings
if (is_ace_enabled) then
{
    ace_map_BFT_Enabled = true;
    ace_map_BFT_Interval = 4;
    ace_map_BFT_HideAIGroups = false;

    ace_map_MapShowCursorCoordinates = true;

    ace_advanced_balistics_Enabled = true;

    ace_medical_EnableRevive = 1;

    BIS_revive_enabled = false;
};

// Add halo action to mapboards.
if (hasInterface) then
{
    [] spawn
    {
        call compile preprocessFileLineNumbers "_halo.sqf";
        mapboard call halo_fnc_setup_object;
        mapboard_1 call halo_fnc_setup_object;
        mapboard_2 call halo_fnc_setup_object;
    };
};

// Dynamic Groups (thank you BIS)
if (isServer && isMultiplayer) then
{
    ["InitializePlayer"] call BIS_fnc_dynamicGroups;
};

if (hasInterface) then
{
    [] spawn
    {
        // JIP guard
        waitUntil {isServer || !isNull player};

        execVM "addBriefing.sqf";

        ["InitializePlayer", [player]] call BIS_fnc_dynamicGroups;

        // TODO, use a param for this and disable it if the ACE map thing is enabled.
        call compile preprocessFileLineNumbers "_audit.sqf";
        cy_audit_loop = [player] call audit_fnc_initLocalUnit;
    };
};

// Curator player registry

if (isServer) then
{
    nowhere_fnc_addPlayerToZeus =
    {
        params ["_unit"];
        zoos addCuratorEditableObjects [[_unit], true];
    };
};

if (hasInterface) then
{
    [] spawn
    {
        // JIP guard
        waitUntil {isServer || !isNull player};
        [player] remoteExec ["nowhere_fnc_addPlayerToZeus", SERVER_CID]
    };
};
