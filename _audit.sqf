#include "cyclone\common\defines.h"

#define UPDATE_INTERVAL 2
#define OBJ_MKR_VAR "audit_mkr"
#define GROUP_MKR_PREFIX "audit_group_mkr"
#define UNIT_MKR_PREFIX "audit_unit_mkr"

audit_fnc_markerName =
{
    scriptName "audit_fnc_markerName";
    params ["_prefix", "_counter"];

    [_prefix, ([_counter] call cy_fnc_Counter_count) call cy_fnc_toBase36] joinString "_"
};

audit_fnc_localLoop =
{
    scriptName "audit_fnc_localLoop";
    params ["_unit"];

    var(_mkr_counter) = [] call cy_fnc_Counter_init;
    var(_group_markers) = [];
    var(_unit_markers) = [];

    while { sleep UPDATE_INTERVAL; true } do
    {
        var(_visible_sides) = ([_unit] call BIS_fnc_friendlySides) - [civilian];

        var(_updated_group_markers) = [];

        {
            var(_mkr) = _x getVariable OBJ_MKR_VAR;
            if (isNil "_mkr") then
            {
                // Group has no marker yet, create it
                _mkr = [GROUP_MKR_PREFIX, _mkr_counter] call audit_fnc_markerName;
                _x setVariable [OBJ_MKR_VAR, _mkr];
                _group_markers pushBack _mkr;

                createMarkerLocal [_mkr, [0, 0, 0]];
                //_mkr setMarkerColorLocal ([side _x, true] call BIS_fnc_sideColor);
                _mkr setMarkerShapeLocal "ICON";
                switch (side _x) do
                {
                case east:       { _mkr setMarkerTypeLocal "o_inf" };
                case resistance: { _mkr setMarkerTypeLocal "n_inf" };
                default          { _mkr setMarkerTypeLocal "b_inf" };
                };
            };
            _mkr setMarkerPosLocal (position leader _x);
            _mkr setMarkerTextLocal (groupId _x);
            _updated_group_markers pushBack _mkr;
        }
        forEach ([ allGroups
                 , {
                    ((_visible_sides find (side _x)) != -1)
                    and {!([] isEqualTo (units _x))}
                   }
                 ] call BIS_fnc_conditionalSelect);

        {
            deleteMarkerLocal _x;
        }
        forEach (_group_markers - _updated_group_markers);

        var(_updated_unit_markers) = [];

        {
            if (!(_x isEqualTo leader group _x)) then
            {
                var(_mkr) = _x getVariable OBJ_MKR_VAR;
                if (isNil "_mkr") then
                {
                    _mkr = [UNIT_MKR_PREFIX, _mkr_counter] call audit_fnc_markerName;
                    _x setVariable [OBJ_MKR_VAR, _mkr];
                    _unit_markers pushBack _mkr;

                    createMarkerLocal [_mkr, [0, 0, 0]];
                    _mkr setMarkerShapeLocal "ICON";
                    _mkr setMarkerTypeLocal "mil_triangle";
                };
                switch (assignedTeam _x) do
                {
                case "RED":    { _mkr setMarkerColorLocal "ColorRED" };
                case "GREEN":  { _mkr setMarkerColorLocal "ColorGREEN" };
                case "BLUE":   { _mkr setMarkerColorLocal "ColorBLUE" };
                case "YELLOW": { _mkr setMarkerColorLocal "ColorYELLOW" };
                default        { _mkr setMarkerColorLocal "ColorBLACK" };
                };
                _mkr setMarkerDirLocal (direction _x);
                _mkr setMarkerPosLocal (position _x);
                //_mkr setMarkerTextLocal (name _x);
                _updated_unit_markers pushBack _mkr;
            };
        }
        forEach units group _unit;

        {
            deleteMarkerLocal _x;
        }
        forEach (_unit_markers - _updated_unit_markers);
    };
};

audit_fnc_initLocalUnit =
{
    scriptName "audit_fnc_initLocalUnit";

    _this spawn audit_fnc_localLoop
};
